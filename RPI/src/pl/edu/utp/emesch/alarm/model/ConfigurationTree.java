package pl.edu.utp.emesch.alarm.model;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ConfigurationTree {
	private Element el_root;
	private NodeList nList_configs;
	
	public ConfigurationTree(Element el_root, NodeList nList_configs){
		setEl_root(el_root);
		this.nList_configs = nList_configs;
	}

	public NodeList getConfigs() {
		return nList_configs;
	}

	public Element get_rootElement() {
		return el_root;
	}

	public void setEl_root(Element el_root) {
		this.el_root = el_root;
	}	
	
}
