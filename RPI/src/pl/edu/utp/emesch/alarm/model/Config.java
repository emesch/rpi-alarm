package pl.edu.utp.emesch.alarm.model;

import java.util.ArrayList;

public class Config {
	private int id;
	private String name;
	private ArrayList<Sensor> arr_Sensors;
	
	int all_response;
	//general sensor configuration
	private int all_keeptime;
	
	public Config(int id, String name, ArrayList<Sensor> arr_Sensors, int all_response, int all_keeptime){
		this.id = id;
		this.name = name;
		this.arr_Sensors = arr_Sensors;		
		this.all_response = all_response;
		this.all_keeptime = all_keeptime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Sensor> getArr_Sensors() {
		return arr_Sensors;
	}

	public void setArr_Sensors(ArrayList<Sensor> arr_Sensors) {
		this.arr_Sensors = arr_Sensors;
	}

	public int getAll_response() {
		return all_response;
	}

	public void setAll_response(int all_response) {
		this.all_response = all_response;
	}

	public int getAll_keeptime() {
		return all_keeptime;
	}

	public void setAll_keeptime(int all_keeptime) {
		this.all_keeptime = all_keeptime;
	}
	
}


