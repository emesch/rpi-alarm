package pl.edu.utp.emesch.alarm.model;

public class Sensor {
	private int id;
	private String name;
	/**
	 * ??
	 */
	private boolean available;
	private int response, keeptime;
	/**
	 * State in zone. ON/OFF
	 */
	private int state;

	/**
	 * Sensor status. 
	 * 0 - off
	 * 1 - event detected
	 */
	private int status;
	/**
	 * Duration of sensor high status;
	 */
	private int status_keeptime;

	/**
	 * Sensor constructor.
	 * Initially unavailable. 
	 * @param id
	 * @param name
	 * @param response
	 * @param keeptime
	 */
	public Sensor (int id, String name, int response, int keeptime, int state){
		this.id = id;
		this.name = name;
		this.response = response;
		this.keeptime = keeptime;
		this.state = state;

		available = false;
		status = 0;
		status_keeptime = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public int getResponse() {
		return response;
	}

	public void setResponse(int response) {
		this.response = response;
	}

	public int getKeeptime() {
		return keeptime;
	}

	public void setKeeptime(int keeptime) {
		this.keeptime = keeptime;
	}

	/**
	 * Is sensor selected in zone? 
	 */
	public int getState() {
		return state;
	}
	
	
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * Something happened on sensor? 
	 */
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		if(status ==0 || status ==1){
			this.status = status;
		}else{
			System.err.println("Sensor.setStatus - WRONG STATUS!!");
		}		
	}

	public int getStatusKeeptime() {
		return status_keeptime;
	}

	public void setStatusKeeptime(int status_keeptime) {
		this.status_keeptime = status_keeptime;
	}






}
