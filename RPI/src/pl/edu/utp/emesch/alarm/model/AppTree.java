package pl.edu.utp.emesch.alarm.model;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class AppTree {
	private Element el_root;
	private NodeList nList_appNodes;
	
	public AppTree(Element el_root, NodeList nList_appNodes){
		this.el_root = el_root;
		this.nList_appNodes = nList_appNodes;
	}

	public NodeList getAppNodes() {
		return nList_appNodes;
	}	
	
}
