package pl.edu.utp.emesch.alarm;

import java.util.ArrayList;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.edu.utp.emesch.alarm.model.AppTree;
import pl.edu.utp.emesch.alarm.model.Config;
import pl.edu.utp.emesch.alarm.model.ConfigurationTree;
import pl.edu.utp.emesch.alarm.model.Sensor;

public class Utils {
	
	public static void loadAppTree(){
		System.out.println("\tLoading App data...");			
		Element el_AppRoot = MainApp.doc_app.getDocumentElement();			
		NodeList nList_appNodes = el_AppRoot.getChildNodes();	
		System.out.println("\t"+nList_appNodes.getLength()+" values loaded.");
		MainApp.tree_app = new AppTree(el_AppRoot, nList_appNodes);
	}
	
	public static void updateAlarm(String alarmCode) {
		System.out.println("\tUpdating alarm code in xml...");
		Node node_alarmCode = MainApp.tree_app.getAppNodes().item(2);
		Element el_alarmCode = (Element)node_alarmCode;
		el_alarmCode.setTextContent(alarmCode);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		DOMSource doc_source = new DOMSource(MainApp.doc_app);
		StreamResult result = new StreamResult(MainApp.file_app);
		try {
			transformer = transformerFactory.newTransformer();
			DOMImplementation domImpl = MainApp.doc_app.getImplementation();
			DocumentType doctype = domImpl.createDocumentType("doctype",
					"app", "app.dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			transformer.transform(doc_source, result);
			System.out.println("\tDone");
		} catch (TransformerException e) {			
			e.printStackTrace();
		}
		
	}

	public static void loadConfigurationTree(){
		System.out.println("\tLoading configurations...");			
		Element el_configRoot = MainApp.doc_config.getDocumentElement();			
		NodeList nList_configs = el_configRoot.getElementsByTagName("config");	
		System.out.println("\t"+nList_configs.getLength()+" saved configurations found.");
		MainApp.tree_configs = new ConfigurationTree(el_configRoot, nList_configs);
	}

	public static void updateZone(int currentConfigID){
		System.out.println("\tUpdating current configuration in xml...");
		Node node_config = MainApp.tree_configs.getConfigs().item(currentConfigID);
		Element el_config = (Element)node_config;

		NodeList nList_configSensors = el_config.getElementsByTagName("sensors").item(0).getChildNodes();
		System.out.println("\t\tBrowsing sensors in configuration...");
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		DOMSource doc_source = new DOMSource(MainApp.doc_config);
		StreamResult result = new StreamResult(MainApp.file_config);
		for(int i=0; i<nList_configSensors.getLength(); i++){
			Node node_sensor = nList_configSensors.item(i);
			Element el_sensor = (Element)node_sensor;
			Node node_sensorState = el_sensor.getElementsByTagName("state").item(0);
			Element el_sensorState = (Element)node_sensorState;
			int sensorState = Integer.parseInt(el_sensorState.getTextContent());
			int sensorStateFromArray = MainApp.arr_configs.get(currentConfigID).getArr_Sensors().get(i).getState();
			System.out.println("\t\tSensor state in XML: "+sensorState);
			System.out.println("\t\tSensor state in array: "+sensorStateFromArray);
			if(sensorState!=sensorStateFromArray){
				el_sensorState.setTextContent(Integer.toString(sensorStateFromArray));
				System.out.println("\t\t!!Corrected to value: "+el_sensorState.getTextContent());
			}
		}//end of for
		
		try {
			transformer = transformerFactory.newTransformer();
			DOMImplementation domImpl = MainApp.doc_config.getImplementation();
			DocumentType doctype = domImpl.createDocumentType("doctype",
					"configurations", "config.dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			transformer.transform(doc_source, result);
		} catch (TransformerException e) {			
			e.printStackTrace();
		}
		System.out.println("Done");
	}
	
	public static void updateZoneInfo(ArrayList<String> textFieldValues) {
		int currentID = Integer.valueOf(MainApp.currentConfigID);
		System.out.println("\tUpdating zone info...");
		Node node_config = MainApp.tree_configs.getConfigs().item(MainApp.currentConfigID);
		Element el_config = (Element)node_config;
		Node node_zoneName = el_config.getElementsByTagName("name").item(0);
		Element el_zoneName = (Element)node_zoneName;
		el_zoneName.setTextContent(textFieldValues.get(0));
		MainApp.arr_configs.get(MainApp.currentConfigID).setName(textFieldValues.get(0));
		MainApp.oList_configNames.set(MainApp.currentConfigID, textFieldValues.get(0));
		MainApp.controller_settings.selectCBIndex(currentID);
		
		NodeList nList_configSensors = el_config.getElementsByTagName("sensors").item(0).getChildNodes();		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		DOMSource doc_source = new DOMSource(MainApp.doc_config);
		StreamResult result = new StreamResult(MainApp.file_config);
		for(int i=0; i<nList_configSensors.getLength(); i++){
			Node node_sensor = nList_configSensors.item(i);
			Element el_sensor = (Element)node_sensor;
			Node node_sensorName = el_sensor.getElementsByTagName("name").item(0);
			Element el_sensorName = (Element)node_sensorName;
			el_sensorName.setTextContent(textFieldValues.get(i+1));
			MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().get(i).setName(textFieldValues.get(i+1));
			MainApp.controller_settings.setSensorButtonName(i, textFieldValues.get(i+1));
			MainApp.controller_details.setSensorLabelName(i, textFieldValues.get(i+1));
		}
		try {
			transformer = transformerFactory.newTransformer();
			DOMImplementation domImpl = MainApp.doc_config.getImplementation();
			DocumentType doctype = domImpl.createDocumentType("doctype",
					"configurations", "config.dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			transformer.transform(doc_source, result);
		} catch (TransformerException e) {			
			e.printStackTrace();
		}
		System.out.println("Done");
		
	}

	public static void saveSensorSettings4All() {
		System.out.println("#saveSensorSettings4All");
		MainApp.controller_sensorSettings.setDisableSaveButton(true);
	}

	public static void saveSensorSettings(int controllerID) {
		System.out.println("SaveSensorSettings for actual sensor number: "+controllerID);
		MainApp.arr_SensorSettingControllers.get(controllerID-1).setDisableSaveButton(true);
		
		int responseTime = MainApp.arr_SensorSettingControllers.get(controllerID-1).getResponseTime();
		int keepTime = MainApp.arr_SensorSettingControllers.get(controllerID-1).getKeepTime();
		MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().get(controllerID-1).setResponse(responseTime);
		MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().get(controllerID-1).setKeeptime(keepTime);
		
		//updateSensorSettingsInXML(controllerID-1); TODO		
	}

	private static void updateSensorSettingsInXML(int sensorID) {
		System.out.println("\tUpdating current Sensor("+sensorID+") in xml...");
		Node node_config = MainApp.tree_configs.getConfigs().item(MainApp.currentConfigID);
		Element el_config = (Element)node_config;
		
		NodeList nList_configSensors = el_config.getElementsByTagName("sensors").item(0).getChildNodes();
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		DOMSource doc_source = new DOMSource(MainApp.doc_config);
		StreamResult result = new StreamResult(MainApp.file_config);
	}

	public static String generateSerialConfigData() {
		MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors();
		String message = "5:";
		String keepTime = "";
		String responseTime = "";
		for(int i=0; i<MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().size(); i++){
			keepTime += MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().get(i).getKeeptime()+":";
			responseTime += MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().get(i).getResponse()+":";
		}
		message += keepTime + responseTime + "\r\n";
		//System.out.println("!!!!!!!!!!!!"+message);
		return message;
	}

	public static void addZone(ArrayList<String> textFieldValues) {
		int currentID = Integer.valueOf(MainApp.currentConfigID);		
		
		System.out.println("\tAdding zone...");
		int id = Integer.valueOf(MainApp.arr_configs.size());
		String name = textFieldValues.get(0);
		ArrayList<Sensor> arr_sensors = new ArrayList<>();
		
		
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		DOMSource doc_source = new DOMSource(MainApp.doc_config);
		StreamResult result = new StreamResult(MainApp.file_config);
		
		Element root = MainApp.doc_config.getDocumentElement();		
		Element rootElement = MainApp.doc_config.getDocumentElement();
		
		Element el_newConfig = MainApp.doc_config.createElement("config");
		Element el_id = MainApp.doc_config.createElement("id");
		el_id.appendChild(MainApp.doc_config.createTextNode(Integer.toString(MainApp.arr_configs.size())));
		el_newConfig.appendChild(el_id);
		Element el_name = MainApp.doc_config.createElement("name");
		el_name.appendChild(MainApp.doc_config.createTextNode(textFieldValues.get(0)));
		el_newConfig.appendChild(el_name);
		Element el_general = MainApp.doc_config.createElement("general");
		Element el_generalResponse = MainApp.doc_config.createElement("response");
		el_generalResponse.appendChild(MainApp.doc_config.createTextNode(Integer.toString(1)));
		Element el_generalKeeptime = MainApp.doc_config.createElement("keeptime");
		el_generalKeeptime.appendChild(MainApp.doc_config.createTextNode(Integer.toString(1)));
		el_general.appendChild(el_generalResponse);
		el_general.appendChild(el_generalKeeptime);
		el_newConfig.appendChild(el_general);
		Element el_sensors = MainApp.doc_config.createElement("sensors");		
		for(int i=0; i<8; i++){
			Element el_sensor = MainApp.doc_config.createElement("sensor");
			Element el_sensorID = MainApp.doc_config.createElement("id");
			el_sensorID.appendChild(MainApp.doc_config.createTextNode(Integer.toString(i)));
			Element el_sensorName = MainApp.doc_config.createElement("name");
			el_sensorName.appendChild(MainApp.doc_config.createTextNode(textFieldValues.get(i+1)));
			Element el_sensorAvailable = MainApp.doc_config.createElement("available");
			el_sensorAvailable.appendChild(MainApp.doc_config.createTextNode(Integer.toString(0)));
			Element el_sensorResponse = MainApp.doc_config.createElement("response");
			el_sensorResponse.appendChild(MainApp.doc_config.createTextNode(Integer.toString(1)));
			Element el_sensorKeeptime = MainApp.doc_config.createElement("keeptime");
			el_sensorKeeptime.appendChild(MainApp.doc_config.createTextNode(Integer.toString(1)));
			Element el_sensorState = MainApp.doc_config.createElement("state");
			el_sensorState.appendChild(MainApp.doc_config.createTextNode(Integer.toString(1)));
			
			el_sensor.appendChild(el_sensorID);
			el_sensor.appendChild(el_sensorName);
			el_sensor.appendChild(el_sensorAvailable);
			el_sensor.appendChild(el_sensorResponse);
			el_sensor.appendChild(el_sensorKeeptime);
			el_sensor.appendChild(el_sensorState);
			
			el_sensors.appendChild(el_sensor);
			arr_sensors.add(new Sensor(i, textFieldValues.get(i+1), 1, 1, 1));
		}
		el_newConfig.appendChild(el_sensors);
		root.appendChild(el_newConfig);		
		MainApp.arr_configs.add(new Config(id, name, arr_sensors, 1, 1));
		MainApp.oList_configNames.add(name);
		MainApp.controller_settings.refreshComboBox();
		try {
			transformer = transformerFactory.newTransformer();
			DOMImplementation domImpl = MainApp.doc_config.getImplementation();
			DocumentType doctype = domImpl.createDocumentType("doctype",
					"configurations", "config.dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			transformer.transform(doc_source, result);
		} catch (TransformerException e) {			
			e.printStackTrace();
		}
		System.out.println("Done");
		
		MainApp.controller_addZone.cleanFields();		
	}

		
	

}
