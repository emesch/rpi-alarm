package pl.edu.utp.emesch.alarm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import pl.edu.utp.emesch.alarm.MainApp;
import pl.edu.utp.emesch.alarm.model.AppTree;
import pl.edu.utp.emesch.alarm.model.Config;
import pl.edu.utp.emesch.alarm.model.ConfigurationTree;
import pl.edu.utp.emesch.alarm.model.Sensor;
import pl.edu.utp.emesch.alarm.serial.SerialController;
import pl.edu.utp.emesch.alarm.view.ActivateAlarmController;
import pl.edu.utp.emesch.alarm.view.DetailsMenuController;
import pl.edu.utp.emesch.alarm.view.ErrorMenuController;
import pl.edu.utp.emesch.alarm.view.FirstRunMenuController;
import pl.edu.utp.emesch.alarm.view.MainMenuController;
import pl.edu.utp.emesch.alarm.view.PreviewMenuController;
import pl.edu.utp.emesch.alarm.view.SensorSettingsMenuController;
import pl.edu.utp.emesch.alarm.view.SerialConfigController;
import pl.edu.utp.emesch.alarm.view.SettingsMenuController;
import pl.edu.utp.emesch.alarm.view.ZoneAddController;
import pl.edu.utp.emesch.alarm.view.ZoneEditController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MainApp extends Application {

	private Stage primaryStage;
	private static BorderPane rootLayout;
	public static AnchorPane menu_FirstRun, menu_Main, menu_Activate, menu_Preview, menu_Settings,
	menu_SensorSettings, menu_SensorSettings1, menu_SensorSettings2, 
	menu_SensorSettings3, menu_SensorSettings4, menu_SensorSettings5, 
	menu_SensorSettings6, menu_SensorSettings7, menu_SensorSettings8, menu_Error, menu_Details, menu_serialConfig,
	menu_editZone, menu_addZone;

	public static ArrayList<Config> arr_configs;		

	private boolean firstRun;
	/**
	 * lastConfigID changed to currentConfigID
	 */
	public static int currentConfigID;	
	private String alarmCode;
	private int sensorCount;
	private static boolean isArmed;

	public static AlarmController alarmController;
	public static Thread thr_alarmContr;

	public static ObservableList<String> oList_configNames;
	private FirstRunMenuController controller_firstRun;
	public static SerialConfigController controller_serialConfig;
	public static MainMenuController controller_mainMenu;
	public static ActivateAlarmController controller_activateAlarm;
	public static PreviewMenuController controller_preview;
	public static DetailsMenuController controller_details;
	public static SettingsMenuController controller_settings;
	public static SensorSettingsMenuController controller_sensorSettings, controller_sensorSettings1, 
	controller_sensorSettings2, controller_sensorSettings3, 
	controller_sensorSettings4, controller_sensorSettings5, 
	controller_sensorSettings6, controller_sensorSettings7, 
	controller_sensorSettings8;
	public static ArrayList<SensorSettingsMenuController> arr_SensorSettingControllers;
	public static ErrorMenuController controller_errorMenu;
	public static ZoneEditController controller_editZone;
	public static ZoneAddController controller_addZone;

	/*
	 * Parsed xml files.
	 */
	public static File file_config, file_app;
	public static Document doc_config, doc_app;
	public static AppTree tree_app;
	public static ConfigurationTree tree_configs;

	public static SerialController port_com;
	public static ObservableList<String> oList_portNames;
	private static boolean isConnected = false;
	
	
	@Override
	public void start(Stage primaryStage) throws IOException {
		this.primaryStage = primaryStage;
		primaryStage.setTitle("RPI Alarm v.0.7.0");

		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent e) {
				Platform.exit();
				System.exit(0);
			}
		});

		setArmed(false);		
		initRootLayout();

		try {
			loadAppData();
		} catch (IOException e) {
			System.err.println("#loadAppData: Problem with loading xml file.");
			e.printStackTrace();
		}
		try {
			loadConfigurations();
		} catch (IOException e) {
			System.err.println("#loadConfiguration: Problem with loading xml file.");
			e.printStackTrace();
		}

		sensorCount = 0;

		initMenus();
		initConfiguration();
		
		//Alarm controller initialization 
		alarmController = new AlarmController(5);
		thr_alarmContr = new Thread(alarmController);
		
		initSerialController();
		
		if(isFirstRun()){
			showFirstRunMenu();
		}else{			
			showSerialConfigMenu();			
		}
	}	


	private void initSerialController() {
		port_com = new SerialController();
		String[] ports = port_com.searchForPorts();
		List<String> list_ports = new ArrayList<String>();
		
		for(int i=0; i<ports.length; i++){
			list_ports.add(ports[i]);
		}
		oList_portNames = FXCollections.observableList(list_ports);
		controller_serialConfig.loadPortList();						
	}

	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Prepare all UI menus. This method contains FXMLLoaders, and Controllers set up.
	 */
	public void initMenus() {
		System.out.println("#MainApp.initMenus():");
		try {
			// FIRST RUN MENU
			FXMLLoader loader_firstRunMenu = new FXMLLoader();
			loader_firstRunMenu.setLocation(MainApp.class.getResource("view/FirstRunMenu.fxml"));
			menu_FirstRun = (AnchorPane) loader_firstRunMenu.load();
			
			// SERIAL CONFIG MENU
			FXMLLoader loader_serialConfig = new FXMLLoader();
			loader_serialConfig.setLocation(MainApp.class.getResource("view/SerialConfigMenu.fxml"));
			menu_serialConfig = (AnchorPane) loader_serialConfig.load();

			// MAIN MENU
			FXMLLoader loader_mainMenu = new FXMLLoader();
			loader_mainMenu.setLocation(MainApp.class.getResource("view/MainMenu.fxml"));
			menu_Main = (AnchorPane) loader_mainMenu.load();

			// ACTIVATE ALARM MENU
			FXMLLoader loader_activateAlarm = new FXMLLoader();
			loader_activateAlarm.setLocation(MainApp.class.getResource("view/ActivateAlarm.fxml"));
			menu_Activate = (AnchorPane) loader_activateAlarm.load();

			// PREVIEW MENU
			FXMLLoader loader_preview = new FXMLLoader();
			loader_preview.setLocation(MainApp.class.getResource("view/PreviewMenu.fxml"));
			menu_Preview = (AnchorPane) loader_preview.load();

			// SETTINGS MENU
			FXMLLoader loader_settings = new FXMLLoader();
			loader_settings.setLocation(MainApp.class.getResource("view/SettingsMenu.fxml"));
			menu_Settings = (AnchorPane) loader_settings.load();

			// SENSOR SETTINGS MENUs
			FXMLLoader loader_sensorSettings = new FXMLLoader();
			loader_sensorSettings.setLocation(MainApp.class.getResource("view/SensorSettingsMenu.fxml"));
			menu_SensorSettings = (AnchorPane) loader_sensorSettings.load();
			FXMLLoader loader_sensorSettings1 = new FXMLLoader();
			loader_sensorSettings1.setLocation(MainApp.class.getResource("view/SensorSettingsMenu.fxml"));
			menu_SensorSettings1 = (AnchorPane) loader_sensorSettings1.load();
			FXMLLoader loader_sensorSettings2 = new FXMLLoader();
			loader_sensorSettings2.setLocation(MainApp.class.getResource("view/SensorSettingsMenu.fxml"));
			menu_SensorSettings2 = (AnchorPane) loader_sensorSettings2.load();
			FXMLLoader loader_sensorSettings3 = new FXMLLoader();
			loader_sensorSettings3.setLocation(MainApp.class.getResource("view/SensorSettingsMenu.fxml"));
			menu_SensorSettings3 = (AnchorPane) loader_sensorSettings3.load();
			FXMLLoader loader_sensorSettings4 = new FXMLLoader();
			loader_sensorSettings4.setLocation(MainApp.class.getResource("view/SensorSettingsMenu.fxml"));
			menu_SensorSettings4 = (AnchorPane) loader_sensorSettings4.load();
			FXMLLoader loader_sensorSettings5 = new FXMLLoader();
			loader_sensorSettings5.setLocation(MainApp.class.getResource("view/SensorSettingsMenu.fxml"));
			menu_SensorSettings5 = (AnchorPane) loader_sensorSettings5.load();
			FXMLLoader loader_sensorSettings6 = new FXMLLoader();
			loader_sensorSettings6.setLocation(MainApp.class.getResource("view/SensorSettingsMenu.fxml"));
			menu_SensorSettings6 = (AnchorPane) loader_sensorSettings6.load();
			FXMLLoader loader_sensorSettings7 = new FXMLLoader();
			loader_sensorSettings7.setLocation(MainApp.class.getResource("view/SensorSettingsMenu.fxml"));
			menu_SensorSettings7 = (AnchorPane) loader_sensorSettings7.load();
			FXMLLoader loader_sensorSettings8 = new FXMLLoader();
			loader_sensorSettings8.setLocation(MainApp.class.getResource("view/SensorSettingsMenu.fxml"));
			menu_SensorSettings8 = (AnchorPane) loader_sensorSettings8.load();
			
			//ERROR MENU
			FXMLLoader loader_errorMenu = new FXMLLoader();
			loader_errorMenu.setLocation(MainApp.class.getResource("view/ErrorMenu.fxml"));
			menu_Error = (AnchorPane) loader_errorMenu.load();
			
			//DETAILS MENU
			FXMLLoader loader_detailsMenu = new FXMLLoader();
			loader_detailsMenu.setLocation(MainApp.class.getResource("view/DetailsMenu.fxml"));
			menu_Details = (AnchorPane) loader_detailsMenu.load();
			
			//EDIT ZONE MENU
			FXMLLoader loader_editZone = new FXMLLoader();
			loader_editZone.setLocation(MainApp.class.getResource("view/ZoneEditMenu.fxml"));
			menu_editZone = (AnchorPane) loader_editZone.load();
			
			//ADD ZONE MENU
			FXMLLoader loader_addZone = new FXMLLoader();
			loader_addZone.setLocation(MainApp.class.getResource("view/ZoneAddMenu.fxml"));
			menu_addZone = (AnchorPane) loader_addZone.load();

			controller_errorMenu = loader_errorMenu.getController();
			controller_errorMenu.setMainApp(this);			
			controller_firstRun = loader_firstRunMenu.getController();
			controller_firstRun.setMainApp(this);
			controller_serialConfig = loader_serialConfig.getController();
			controller_serialConfig.setMainApp(this);
			controller_mainMenu = loader_mainMenu.getController();
			controller_mainMenu.setMainApp(this);
			controller_activateAlarm = loader_activateAlarm.getController();
			controller_activateAlarm.setMainApp(this);
			controller_preview = loader_preview.getController();
			controller_preview.setMainApp(this);
			controller_details = loader_detailsMenu.getController();
			controller_details.setMainApp(this);
			controller_editZone = loader_editZone.getController();
			controller_editZone.setMainApp(this);
			controller_addZone = loader_addZone.getController();
			controller_addZone.setMainApp(this);
			
			controller_settings = loader_settings.getController();
			controller_settings.setMainApp(this);
			controller_sensorSettings = loader_sensorSettings.getController();
			controller_sensorSettings.setMainApp(this);
			controller_sensorSettings.setControllerID(0);
			controller_sensorSettings1 = loader_sensorSettings1.getController();
			controller_sensorSettings1.setMainApp(this);
			controller_sensorSettings1.setControllerID(1);
			controller_sensorSettings2 = loader_sensorSettings2.getController();
			controller_sensorSettings2.setMainApp(this);
			controller_sensorSettings2.setControllerID(2);
			controller_sensorSettings3 = loader_sensorSettings3.getController();
			controller_sensorSettings3.setMainApp(this);
			controller_sensorSettings3.setControllerID(3);
			controller_sensorSettings4 = loader_sensorSettings4.getController();
			controller_sensorSettings4.setMainApp(this);
			controller_sensorSettings4.setControllerID(4);
			controller_sensorSettings5 = loader_sensorSettings5.getController();
			controller_sensorSettings5.setMainApp(this);
			controller_sensorSettings5.setControllerID(5);
			controller_sensorSettings6 = loader_sensorSettings6.getController();
			controller_sensorSettings6.setMainApp(this);
			controller_sensorSettings6.setControllerID(6);
			controller_sensorSettings7 = loader_sensorSettings7.getController();
			controller_sensorSettings7.setMainApp(this);
			controller_sensorSettings7.setControllerID(7);
			controller_sensorSettings8 = loader_sensorSettings8.getController();
			controller_sensorSettings8.setMainApp(this);
			controller_sensorSettings8.setControllerID(8);
			arr_SensorSettingControllers = new ArrayList<>();
			arr_SensorSettingControllers.add(controller_sensorSettings1);
			arr_SensorSettingControllers.add(controller_sensorSettings2);
			arr_SensorSettingControllers.add(controller_sensorSettings3);
			arr_SensorSettingControllers.add(controller_sensorSettings4);
			arr_SensorSettingControllers.add(controller_sensorSettings5);
			arr_SensorSettingControllers.add(controller_sensorSettings6);
			arr_SensorSettingControllers.add(controller_sensorSettings7);
			arr_SensorSettingControllers.add(controller_sensorSettings8);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadAppData() throws IOException{
		System.out.println("#MainApp.loadAppData():");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setValidating(true);
		dbFactory.setIgnoringElementContentWhitespace(true);

		try {			
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();			

			dBuilder.setErrorHandler(new ErrorHandler() {

				@Override
				public void error(SAXParseException arg0) throws SAXException {
					System.err.println("!!Error: SAXParseException");
					System.err.println("Something had gone wrong in dBuilder. Check the parsers configuration...");
					//arg0.printStackTrace();
				}

				@Override
				public void fatalError(SAXParseException arg0)
						throws SAXException {
					System.err.println("!!Fatal Error: SAXParseException");
					System.err.println("Something had gone wrong in dBuilder. Check the parsers configuration...");					
				}

				@Override
				public void warning(SAXParseException arg0) throws SAXException {
					System.err.println("!!Warning: SAXParseException");
					System.err.println("Something had gone wrong in dBuilder. Check the parsers configuration...");					
				}

			});//end of setErrorHandler()

			file_app = new File("xml/app.xml");									
			doc_app = dBuilder.parse(file_app);
			System.out.println("\tAppData file loaded.");

			Utils.loadAppTree();			
			setFirstRun(Boolean.parseBoolean(tree_app.getAppNodes().item(0).getTextContent()));
			currentConfigID = Integer.parseInt(tree_app.getAppNodes().item(1).getTextContent());
			alarmCode = tree_app.getAppNodes().item(2).getTextContent();


		}catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}		
	}

	/**
	 * Load all saved configurations.
	 * Set up the last program configuration from config.xml file.
	 */
	private void loadConfigurations() throws IOException {
		System.out.println("#MainApp.loadConfigurations():");
		arr_configs = new ArrayList<>();


		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setValidating(true);
		dbFactory.setIgnoringElementContentWhitespace(true);		

		try {			
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();			

			dBuilder.setErrorHandler(new ErrorHandler() {

				@Override
				public void error(SAXParseException arg0) throws SAXException {
					System.err.println("!!Error: SAXParseException");
					System.err.println("Something had gone wrong in dBuilder. Check the parsers configuration...");
					//arg0.printStackTrace();
				}

				@Override
				public void fatalError(SAXParseException arg0)
						throws SAXException {
					System.err.println("!!Fatal Error: SAXParseException");
					System.err.println("Something had gone wrong in dBuilder. Check the parsers configuration...");					
				}

				@Override
				public void warning(SAXParseException arg0) throws SAXException {
					System.err.println("!!Warning: SAXParseException");
					System.err.println("Something had gone wrong in dBuilder. Check the parsers configuration...");					
				}

			});//end of setErrorHandler() 

			file_config = new File("xml/config.xml");									
			doc_config = dBuilder.parse(file_config);
			System.out.println("\tConfiguration file loaded.");			


			Utils.loadConfigurationTree();			

			List<String> list_settingsNames = new ArrayList<String>();
			//Browsing configurations
			for (int i=0; i<tree_configs.getConfigs().getLength(); i++) {				
				Node node_config = tree_configs.getConfigs().item(i);
				Element el_config = (Element)node_config;

				Node node_configID = el_config.getElementsByTagName("id").item(0);
				Element el_configID = (Element)node_configID;
				int configID = Integer.parseInt(el_configID.getTextContent());
				Node node_configName = el_config.getElementsByTagName("name").item(0);
				Element el_configName = (Element)node_configName;
				String configName =  el_configName.getTextContent();


				NodeList nList_configGeneral = el_config.getElementsByTagName("general").item(0).getChildNodes();
				Element el_allKeeptime = (Element)nList_configGeneral.item(0);
				int allKeeptime = Integer.parseInt(el_allKeeptime.getTextContent());
				Element el_allResponse = (Element)nList_configGeneral.item(1);
				int allResponse = Integer.parseInt(el_allResponse.getTextContent());

				NodeList nList_configSensors = el_config.getElementsByTagName("sensors").item(0).getChildNodes();
				ArrayList<Sensor> arr_Sensors = new ArrayList<>();				
				for (int j=0; j<nList_configSensors.getLength(); j++) {
					Node node_sensor = nList_configSensors.item(j);
					Element el_sensor = (Element)node_sensor;

					Node node_sensorID = el_sensor.getElementsByTagName("id").item(0);
					Element el_sensorID = (Element)node_sensorID;
					int sensorID = Integer.parseInt(el_sensorID.getTextContent());
					Node node_sensorName = el_sensor.getElementsByTagName("name").item(0);
					Element el_sensorName = (Element)node_sensorName;
					String sensorName = el_sensorName.getTextContent();
					Node node_sensorResponse = el_sensor.getElementsByTagName("response").item(0);
					Element el_sensorResponse = (Element)node_sensorResponse;
					int sensorResponse = Integer.parseInt(el_sensorResponse.getTextContent());
					Node node_sensorKeeptime = el_sensor.getElementsByTagName("keeptime").item(0);
					Element el_sensorKeeptime = (Element)node_sensorKeeptime;
					int sensorKeeptime = Integer.parseInt(el_sensorKeeptime.getTextContent());
					Node node_sensorState = el_sensor.getElementsByTagName("state").item(0);
					Element el_sensorState = (Element)node_sensorState;
					int sensorState = Integer.parseInt(el_sensorState.getTextContent());

					//System.out.println("Adding new sensor: "+sensorID+sensorName+sensorResponse+sensorKeeptime);
					arr_Sensors.add(new Sensor(sensorID, sensorName, sensorResponse, sensorKeeptime, sensorState));
				}
				arr_configs.add(new Config(configID, configName, arr_Sensors, allResponse, allKeeptime));
				list_settingsNames.add(arr_configs.get(i).getName());				
			}
			oList_configNames = FXCollections.observableList(list_settingsNames);			

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		System.out.println("\t"+arr_configs.size()+" saved configurations loaded.");

	}//end of loadConfigurations()

	/**
	 * 1)Set allResponseTimeLabel and allKeepTimeLabel
	 * 2)initSensors()  
	 */
	public void initConfiguration() {
		System.out.println("#MainApp.initConfiguration():");
		int allResponseTime = arr_configs.get(currentConfigID).getAll_response();
		int allkeepTime = arr_configs.get(currentConfigID).getAll_keeptime();
		String configName = arr_configs.get(currentConfigID).getName();

		controller_mainMenu.setZone(configName);

		controller_sensorSettings.setResponseTime(allResponseTime);		
		controller_sensorSettings.setKeepTime(allkeepTime);
		controller_sensorSettings.setSettingsName("WSZYSTKIE CZUNIKI");
		controller_sensorSettings.setScrollBarValue(0, controller_sensorSettings.getResponseTime());
		controller_sensorSettings.setScrollBarValue(1, controller_sensorSettings.getKeepTime());
		

		controller_preview.setZoneName(configName);
		controller_preview.setSensorCount(sensorCount);

		initSensors();		
	}

	/**
	 * Initializes sensor settings 
	 */
	private void initSensors() {
		System.out.println("#MainApp.initSensors():");
		for(int i=0; i<arr_configs.get(currentConfigID).getArr_Sensors().size(); i++){
			String sensorName = arr_configs.get(currentConfigID).getArr_Sensors().get(i).getName();
			controller_settings.setSensorButtonName(i, sensorName);
			controller_details.setSensorLabelName(i, sensorName);
			controller_settings.setSensorState(i, arr_configs.get(currentConfigID).getArr_Sensors().get(i).getState());
			int responseTime = arr_configs.get(currentConfigID).getArr_Sensors().get(i).getResponse();
			arr_SensorSettingControllers.get(i).setResponseTime(responseTime);
			int keepTime = arr_configs.get(currentConfigID).getArr_Sensors().get(i).getKeeptime();
			arr_SensorSettingControllers.get(i).setKeepTime(keepTime);
			arr_SensorSettingControllers.get(i).setSettingsName("CZUJNIK ID = "+i);
			arr_SensorSettingControllers.get(i).setScrollBarValue(0, responseTime);
			arr_SensorSettingControllers.get(i).setScrollBarValue(1, keepTime);			
		}
	}

	private void showFirstRunMenu() {
		rootLayout.setCenter(menu_FirstRun);		
	}
	
	public void showSerialConfigMenu() {
		rootLayout.setCenter(menu_serialConfig);		
	}


	public static void showMainMenu() {
		Platform.runLater(new Runnable(){
			@Override
			public void run() {
				rootLayout.setCenter(menu_Main);
			}			
		});			
	}

	public void showActivateMenu() {
		rootLayout.setCenter(menu_Activate);
	}

	public void showPreviewMenu() {
		rootLayout.setCenter(menu_Preview);		
	}

	public void showSettingsMenu() {
		rootLayout.setCenter(menu_Settings);		
	}
	
	public static void showErrorMenu() {
		Platform.runLater(new Runnable(){

			@Override
			public void run() {
				rootLayout.setCenter(menu_Error);
			}
			
		});		
	}
	
	public void showDetailsMenu() {
		rootLayout.setCenter(menu_Details);		
	}

	/**
	 * Show sensor settings menu. 
	 * @param sensorNumber
	 * If 0, then set all sensors
	 */
	public void showSensorSettings(int sensorNumber) {
		switch(sensorNumber){
		case 0: rootLayout.setCenter(menu_SensorSettings);
		break;
		case 1: rootLayout.setCenter(menu_SensorSettings1);
		break;
		case 2: rootLayout.setCenter(menu_SensorSettings2);
		break;
		case 3: rootLayout.setCenter(menu_SensorSettings3);
		break;
		case 4: rootLayout.setCenter(menu_SensorSettings4);
		break;
		case 5: rootLayout.setCenter(menu_SensorSettings5);
		break;
		case 6: rootLayout.setCenter(menu_SensorSettings6);
		break;
		case 7: rootLayout.setCenter(menu_SensorSettings7);
		break;
		case 8: rootLayout.setCenter(menu_SensorSettings8);
		break;
		}

	}
	
	public void showAddZoneMenu() {
		rootLayout.setCenter(menu_addZone);
	}
	
	public void showEditZoneMenu() {
		rootLayout.setCenter(menu_editZone);		
	}

	public static void main(String[] args) {
		launch(args);
	}


	public ArrayList<Config> getArr_configs() {
		return arr_configs;
	}

	public String getAlarmCode(){
		return alarmCode;
	}

	public void setAlarmCode(String alarmCode){
		this.alarmCode = alarmCode;
	}

	public static void setArmedAlarmStyle(){
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				menu_Main.getStylesheets().clear();
				menu_Activate.getStylesheets().clear();
				menu_Preview.getStylesheets().clear();
				menu_SensorSettings.getStylesheets().clear();
				menu_SensorSettings1.getStylesheets().clear();
				menu_SensorSettings2.getStylesheets().clear();
				menu_SensorSettings3.getStylesheets().clear();
				menu_SensorSettings4.getStylesheets().clear();
				menu_SensorSettings5.getStylesheets().clear();
				menu_SensorSettings6.getStylesheets().clear();
				menu_SensorSettings7.getStylesheets().clear();
				menu_SensorSettings8.getStylesheets().clear();
				menu_Settings.getStylesheets().clear();
				menu_Main.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_Activate.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_Preview.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_SensorSettings.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_SensorSettings1.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_SensorSettings2.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_SensorSettings3.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_SensorSettings4.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_SensorSettings5.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_SensorSettings6.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_SensorSettings7.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_SensorSettings8.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());
				menu_Settings.getStylesheets().add(MainApp.class.getResource("view/Theme2.css").toExternalForm());

				controller_mainMenu.setArmed();
				controller_activateAlarm.setArmed();	

			}			
		});

	}

	public static void setDisarmedAlarmStyle(){
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				menu_Main.getStylesheets().clear();
				menu_Activate.getStylesheets().clear();
				menu_Preview.getStylesheets().clear();
				menu_SensorSettings.getStylesheets().clear();
				menu_SensorSettings1.getStylesheets().clear();
				menu_SensorSettings2.getStylesheets().clear();
				menu_SensorSettings3.getStylesheets().clear();
				menu_SensorSettings4.getStylesheets().clear();
				menu_SensorSettings5.getStylesheets().clear();
				menu_SensorSettings6.getStylesheets().clear();
				menu_SensorSettings7.getStylesheets().clear();
				menu_SensorSettings8.getStylesheets().clear();
				menu_Settings.getStylesheets().clear();
				menu_Main.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_Activate.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_Preview.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_SensorSettings.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_SensorSettings1.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_SensorSettings2.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_SensorSettings3.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_SensorSettings4.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_SensorSettings5.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_SensorSettings6.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_SensorSettings7.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_SensorSettings8.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());
				menu_Settings.getStylesheets().add(MainApp.class.getResource("view/Theme1.css").toExternalForm());

				controller_mainMenu.setDisarmed();
				controller_activateAlarm.setDisarmed();
			}			
		});

	}

	public static void setWrongCodeStyle(){
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				menu_Activate.getStylesheets().clear();
				menu_Activate.getStylesheets().add(MainApp.class.getResource("view/ThemeWrongCode.css").toExternalForm());				
			}
		});		
	}

	public static void handleArming(int state){
		Platform.runLater(new Runnable(){

			@Override
			public void run() {
				switch(state){
				//isArming
				case 0: controller_mainMenu.setDisableActivateButton(true);
				break;
				//armed
				case 1: controller_mainMenu.setDisableActivateButton(false);
				break;				
				}
			}
		});

	}


	public boolean isFirstRun() {
		return firstRun;
	}


	public void setFirstRun(boolean firstRun) {
		//TODO: Update in xml
		this.firstRun = firstRun;
	}


	public static boolean isArmed() {
		return isArmed;
	}


	public static void setArmed(boolean isArmed) {
		MainApp.isArmed = isArmed;
	}


	public static boolean isConnected() {
		return isConnected;
	}


	public static void setConnected(boolean isConnected) {
		MainApp.isConnected = isConnected;
	}


	


	

	


}
