package pl.edu.utp.emesch.alarm;

import gnu.io.CommPortIdentifier;

import java.util.Timer;
import java.util.TimerTask;

import com.sun.webkit.ContextMenu.ShowContext;

import pl.edu.utp.emesch.alarm.serial.SerialController;

public class AlarmController implements Runnable{
	//private String alarmCode;
	private int codeChances, timeToLeave;
	private boolean isArmed;
	private Timer timer;
	private boolean isAVRConfigured;
	private boolean isWaitingForResponse;

	private Thread thread_eventController;
	private EventController eventController;

	/**
	 * COM8 emulate RPI Serial Port.
	 */
	private SerialController com8;

	public AlarmController(int timeToLeave){		
		this.timeToLeave = timeToLeave;

		codeChances = 2;
		isArmed = false;
		isAVRConfigured = false;
		isWaitingForResponse = false;		
	}

	@Override
	public void run() {
		try {
			initEventController();
			initSerialController();
			listen();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}

	private void initEventController() {
		eventController = new EventController();
		thread_eventController = new Thread(eventController);
		thread_eventController.start();
	}

	private void initSerialController() {
//		com8 = new SerialController();
//		String[] ports = com8.searchForPorts();
//		com8.connect("COM8");
//		com8.initListener();
//		com8.initIOStream();
		
		

		connectWithAVR();		
	}

	/**
	 * Tries to sendConfigData() in new thread.
	 */
	private void connectWithAVR() {
		Thread thread = new Thread(new Runnable(){
			@Override
			public void run() {
				while(!isAVRConfigured){
					sendConfigData();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}							
			}			
		});
		thread.start();		
	}

	public void sendConfigData() {		
		String configData = Utils.generateSerialConfigData();
		MainApp.port_com.writeLine(configData);
		//MainApp.port_com.writeLine("6:1:1:1:1:1:1:1:1:\r\n");
		MainApp.controller_settings.sendMode();
		//		try {
		//			return waitForAVRConfirm();
		//		} catch (InterruptedException e) {
		//			e.printStackTrace();
		//			return false;
		//		}		
	}

	//TODO: waiting for real confirm
	//	private boolean waitForAVRConfirm() throws InterruptedException {
	//		int i = 0;
	//		while(!isAVRConfigured){
	//			Thread.sleep(100);
	//			i++;
	//			if(i==100){
	//				i=0;
	//				return false;				
	//			}
	//		}		
	//		return true;		
	//	}

	private void listen() throws InterruptedException {		
		if(isArmed){
			System.out.println("###AlarmController: Listening for events...");
			MainApp.setArmedAlarmStyle();
			while(isArmed){
				if(!isWaitingForResponse){
					checkSensors();
				}				
				System.out.println("...");
				Thread.sleep(500);
			}
			System.out.println("###AlarmController: Alarm disarmed...");
			listen();
		}else{
			MainApp.setDisarmedAlarmStyle();
			if(isAVRConfigured){
				while(!isArmed){
					System.out.println("It's not Armed...");
					if(!isWaitingForResponse){
						checkSensors();						
					}
					Thread.sleep(500);
				}
			}else{
				System.err.println("AVR is not configured!");
				Thread.sleep(500);
				handleConnectionProblem();
			}			
		}listen();
		//System.out.println("###AlarmController.listen(): Waiting for permission to listen...");
		//Thread.sleep((timeToLeave*1000)+2000);			
	}


	private void handleConnectionProblem() throws InterruptedException {
		String errorMSG = "----MESSAGE----";	
		if(!isAVRConfigured){
			errorMSG += "\nAVR is not configured!";
		}
		MainApp.controller_errorMenu.setErrorMSG(errorMSG);
		MainApp.controller_mainMenu.setDisableActivateButton(true);
		String remembered_status = MainApp.controller_mainMenu.getStatus();
		MainApp.controller_mainMenu.setStatus("ERROR");
		MainApp.showErrorMenu();
		while(!isAVRConfigured){
			System.err.println("#handleConnectionProblem()");
			Thread.sleep(1000);			
		}
		MainApp.controller_mainMenu.setDisableActivateButton(false);
		MainApp.controller_mainMenu.setStatus(remembered_status);
		MainApp.showMainMenu();
		errorMSG = "";
	}

	//TODO: checking sensors... something wrong with threads...
	private void checkSensors() throws InterruptedException {		

		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				System.out.println("###AlarmController: Checking sensors...");
				setWaitingForResponse(true);
				MainApp.port_com.writeLine("7:\r\n");

				int i = 0;				
				while(isWaitingForResponse){
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					i++;
					if(i==1000){
						if(isArmed){
							turnOnTheAlarm();
							setWaitingForResponse(false);						
						}else{
							System.err.println("###AlarmController: AVR not responding");
							i=0;
							setWaitingForResponse(false);	
							break;
						}
					}
				}
				i=0;
			}

		});
		thread.start();
	}

	public void arm(){
		if(!MainApp.isArmed()){
			MainApp.setArmed(true);
			System.out.println("###AlarmController: Arming the alarm...");
			MainApp.handleArming(0);
			int rememberedTimeToLeave = Integer.valueOf(timeToLeave);
			MainApp.controller_mainMenu.setStatus("UZBRAJANIE...");		
			timer = new Timer();
			timer.scheduleAtFixedRate(new TimerTask() {

				public void run() {
					System.out.println(setInterval());
					if(timeToLeave == 0){	            	
						isArmed = true;	            	
						MainApp.controller_mainMenu.setStatus("UZBROJONY");
						System.out.println("###AlarmController: Armed");
						MainApp.handleArming(1);
						timeToLeave = rememberedTimeToLeave;	            	
					}
				}
			}, 1000, 1000);	
		}
	}

	public void disarm() {
		isArmed = false;
		MainApp.setArmed(false);		
		MainApp.controller_mainMenu.setStatus("ROZBROJONY");
	}

	private final int setInterval() {
		if (timeToLeave == 1)
			timer.cancel();
		MainApp.controller_mainMenu.setStatus(Integer.toString(timeToLeave-1));
		return --timeToLeave;
	}

	public static void main(String args[]){
		/*AlarmController run_alarmController = new AlarmController(5);
		run_alarmController.arm();
		Thread test1 = new Thread (run_alarmController);
		test1.start();
		 */
	}

	public int getCodeChances() {
		return codeChances;
	}

	public void setCodeChances(int codeChances) {
		this.codeChances = codeChances;
	}

	public static void turnOnTheAlarm() {		
		MainApp.controller_mainMenu.setStatus("ALARM!!");
		MainApp.showMainMenu();
	}

	public boolean isAVRConfigured() {
		return isAVRConfigured;
	}

	public void setAVRConfigured(boolean isAVRConfigured) {
		this.isAVRConfigured = isAVRConfigured;
	}

	public void updateSensorState(String[] tokens) {
		//System.err.println("#AlarmController#updateSensorState: tokens size: "+tokens.length);	
		Thread thread = new Thread(new Runnable(){
			@Override
			public void run() {
				//System.out.println("||SENSOR STATES||");
				//Reading sensor states
				for(int i=1; i<=16; i++){
					if(i<=8){
						int sensorStatus = Integer.valueOf(tokens[i]);
						//System.out.print("["+sensorStatus+"]");
						MainApp.controller_details.updateDetails(i-1, tokens[i]);
						eventController.updateSensorStatus(i-1, sensorStatus);
						MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().get(i-1).setStatus(sensorStatus);
						if(sensorStatus==1){
							if(MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().get(i-1).getState()==1 && isArmed){
								handleEvent(i-1);
							}
							//reading keeptime;			
						}else if(i<=16){
							int statusKeeptime = Integer.valueOf(tokens[i]);
							//System.out.print("["+statusKeeptime+"]");
							MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().get(i-1).setStatusKeeptime(statusKeeptime);
						}
					}
				}
				System.out.println();
				setWaitingForResponse(false);				
			}			
		});
		thread.start();

	}

	private void handleEvent(int sensorID) {
		eventController.setRunning(true);
		eventController.handleEvent(sensorID);
	}

	public boolean isWaitingForResponse() {
		return isWaitingForResponse;
	}

	public void setWaitingForResponse(boolean isWaitingForResponse) {
		System.out.println("###AlarmController: setWaitingForResponse: "+isWaitingForResponse);
		this.isWaitingForResponse = isWaitingForResponse;
	}



}


