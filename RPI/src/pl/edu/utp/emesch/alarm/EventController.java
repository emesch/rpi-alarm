package pl.edu.utp.emesch.alarm;

import java.util.ArrayList;

public class EventController implements Runnable{
	/**
	 * When event detected, then isRunning. 
	 */
	private boolean isRunning;
	ArrayList<Integer> arr_sensorStatus, arr_eventCounters;
	
	public static int maxCounterValue = 20;
	
	EventController(){
		this.arr_sensorStatus = new ArrayList<>();
		this.arr_eventCounters = new ArrayList<>();
		for(int i=0; i<8; i++){
			arr_sensorStatus.add(0);
			arr_eventCounters.add(0);
		}
		
		isRunning = false;
	}
	
	@Override
	public void run() {		
		try {
			watch();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}

	private void watch() throws InterruptedException {		
		while(isRunning){
			System.err.println("I am watching!");
			Thread.sleep(1000);
			for(int i=0; i<8; i++){
				int counterValue = arr_eventCounters.get(i);
				int sensorStatus = arr_sensorStatus.get(i);
				if(counterValue!=0 && sensorStatus ==0){
					arr_eventCounters.set(i, 0);
				}
				if(counterValue>maxCounterValue){
					AlarmController.turnOnTheAlarm();
					arr_eventCounters.set(i, 0);
					setRunning(false);
				}
			}
		}
		//Watching for changing sensor status from 1 to 0
		for(int i=0; i<8; i++){
			int counterValue = arr_eventCounters.get(i);
			int sensorStatus = arr_sensorStatus.get(i);
			if(counterValue!=0 && sensorStatus ==0){
				arr_eventCounters.set(i, 0);
			}
		}
		Thread.sleep(1000);
		watch();
	}

	public void setRunning(boolean isRunning) {		
		this.isRunning = isRunning;		
	}

	public void updateSensorStatus(int i, int sensorStatus) {
		arr_sensorStatus.set(i, sensorStatus);		
	}
	
	public void handleEvent(int sensorID){		
		arr_eventCounters.set(sensorID, arr_eventCounters.get(sensorID)+1);
	}
}
