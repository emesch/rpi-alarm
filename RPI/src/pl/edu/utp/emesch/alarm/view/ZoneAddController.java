package pl.edu.utp.emesch.alarm.view;

import java.util.ArrayList;

import pl.edu.utp.emesch.alarm.MainApp;
import pl.edu.utp.emesch.alarm.Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;

public class ZoneAddController {

	@FXML
	private Button button_goBack;
	@FXML
	private Button button_add;
	@FXML
	private TextField label_zoneName;
	@FXML
	private TextField label_sensor0;
	@FXML
	private TextField label_sensor1;
	@FXML
	private TextField label_sensor2;
	@FXML
	private TextField label_sensor3;
	@FXML
	private TextField label_sensor4;
	@FXML
	private TextField label_sensor5;
	@FXML
	private TextField label_sensor6;
	@FXML
	private TextField label_sensor7;

	//Reference to the main application
	private MainApp mainApp;

	
	/**
	 * The constructor.
	 * The constructor is called before the initialize() method.
	 */
	public ZoneAddController() {    	 
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */

	@FXML
	private void initialize() {	
		button_add.setDisable(true);
		
		label_zoneName.textProperty().addListener((observable, oldValue, newValue) -> {
			button_add.setDisable(false);
		});
		
		label_sensor0.textProperty().addListener((observable, oldValue, newValue) -> {
			button_add.setDisable(false);
		});
		label_sensor1.textProperty().addListener((observable, oldValue, newValue) -> {
			button_add.setDisable(false);
		});
		label_sensor2.textProperty().addListener((observable, oldValue, newValue) -> {
			button_add.setDisable(false);
		});
		label_sensor3.textProperty().addListener((observable, oldValue, newValue) -> {
			button_add.setDisable(false);
		});
		label_sensor4.textProperty().addListener((observable, oldValue, newValue) -> {
			button_add.setDisable(false);
		});
		label_sensor5.textProperty().addListener((observable, oldValue, newValue) -> {
			button_add.setDisable(false);
		});
		label_sensor6.textProperty().addListener((observable, oldValue, newValue) -> {
			button_add.setDisable(false);
		});
		label_sensor7.textProperty().addListener((observable, oldValue, newValue) -> {
			button_add.setDisable(false);
		});
		
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;        
	}

	@FXML
	private void handleAdd(){
		ArrayList<String> textFieldValues = new ArrayList<>();
		textFieldValues.add(label_zoneName.getText());
		textFieldValues.add(label_sensor0.getText());
		textFieldValues.add(label_sensor1.getText());
		textFieldValues.add(label_sensor2.getText());
		textFieldValues.add(label_sensor3.getText());
		textFieldValues.add(label_sensor4.getText());
		textFieldValues.add(label_sensor5.getText());
		textFieldValues.add(label_sensor6.getText());
		textFieldValues.add(label_sensor7.getText());
		
		Utils.addZone(textFieldValues);
		mainApp.showSettingsMenu();
	}
	
	@FXML
	private void handle_goBack(){
		button_add.setDisable(true);
		mainApp.showSettingsMenu();
	}

	public void cleanFields() {
		label_zoneName.setText("");
		label_sensor0.setText("");
		label_sensor1.setText("");
		label_sensor2.setText("");
		label_sensor3.setText("");
		label_sensor4.setText("");
		label_sensor5.setText("");
		label_sensor6.setText("");
		label_sensor7.setText("");
	}

}
