package pl.edu.utp.emesch.alarm.view;

import pl.edu.utp.emesch.alarm.MainApp;
import pl.edu.utp.emesch.alarm.Utils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

public class FirstRunMenuController {

	@FXML
	private Button button_ok;	
	@FXML
	private Label label_info;
	@FXML
	private PasswordField pass_oldCode;
	@FXML
	private PasswordField pass_newCode;
	@FXML
	private PasswordField pass_repeatedCode;

	//Reference to the main application
	private MainApp mainApp;

	/**
	 * The constructor.
	 * The constructor is called before the initialize() method.
	 */
	public FirstRunMenuController() {
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */

	@FXML
	private void initialize() {

	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;        
	}

	@FXML
	private void handleOK() {
		System.out.println("#handleButton: Confirm");    	
		String oldCode = pass_oldCode.getText();
		String newCode = pass_newCode.getText();
		String repeatedNewCode = pass_repeatedCode.getText();
		if(mainApp.getAlarmCode().equals(oldCode)){
			if(newCode.equals(repeatedNewCode)){
				if(newCode.equals("")){
					label_info.setText("Nie wpisales nowego kodu!");
				}else {
					mainApp.setAlarmCode(newCode);
					Utils.updateAlarm(newCode);					
					mainApp.setFirstRun(false);
					mainApp.showSerialConfigMenu();	
				}
			}else{
				label_info.setText("Podane kody roznia sie.");
			}
		}else {
			label_info.setText("Podales zly stary kod!");
		}

	}




}
