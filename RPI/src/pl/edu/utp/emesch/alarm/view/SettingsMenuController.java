package pl.edu.utp.emesch.alarm.view;

import java.util.ArrayList;

import pl.edu.utp.emesch.alarm.MainApp;
import pl.edu.utp.emesch.alarm.Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;

public class SettingsMenuController {

	@FXML
	private ComboBox<String> cBox_Settings;
	@FXML
	private Button button_addConfig;
	@FXML
	private Button button_deleteConfig;
	@FXML
	private Button button_saveConfig;
	@FXML
	private Button button_goBack;
	@FXML
	private Button button_editZone;
	@FXML
	private Button button_setAll;
	@FXML
	private Button button_setS1;
	@FXML
	private Button button_setS2;
	@FXML
	private Button button_setS3;
	@FXML
	private Button button_setS4;
	@FXML
	private Button button_setS5;
	@FXML
	private Button button_setS6;
	@FXML
	private Button button_setS7;
	@FXML
	private Button button_setS8;
	@FXML
	private ToggleButton tbutton_S1;
	@FXML
	private ToggleButton tbutton_S2;
	@FXML
	private ToggleButton tbutton_S3;
	@FXML
	private ToggleButton tbutton_S4;
	@FXML
	private ToggleButton tbutton_S5;
	@FXML
	private ToggleButton tbutton_S6;
	@FXML
	private ToggleButton tbutton_S7;
	@FXML
	private ToggleButton tbutton_S8;
	@FXML
	private Button button_mode;


	private ArrayList<ToggleButton> arr_ToggleButtons;

	/**
	 * 1 - normal open
	 * 0 - normal close
	 */
	private int mode;


	//Reference to the main application
	private MainApp mainApp;

	public static int selectionCounter = 0;

	/**
	 * The constructor.
	 * The constructor is called before the initialize() method.
	 */
	public SettingsMenuController() {    	 
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */

	@FXML
	private void initialize() {
		arr_ToggleButtons = new ArrayList<>();
		arr_ToggleButtons.add(tbutton_S1);
		arr_ToggleButtons.add(tbutton_S2);
		arr_ToggleButtons.add(tbutton_S3);
		arr_ToggleButtons.add(tbutton_S4);
		arr_ToggleButtons.add(tbutton_S5);
		arr_ToggleButtons.add(tbutton_S6);
		arr_ToggleButtons.add(tbutton_S7);
		arr_ToggleButtons.add(tbutton_S8);


		cBox_Settings.setItems(MainApp.oList_configNames);
		System.out.println("\t#controller_settings.initialize(): Selecting last config...");
		cBox_Settings.getSelectionModel().select(MainApp.currentConfigID);

		button_saveConfig.setDisable(true);

		mode = 0;
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;        
	}

	@FXML
	private void handleAddConfig() {
		mainApp.showAddZoneMenu();		
	}

	@FXML
	private void handleSaveConfig() {
		System.out.println("#handleButton: Save Zone Configuration.");
		System.out.println("\tUpdating current configuration in array...");		
		//Browsing current configuration sensors, updating sensor state (ON/OFF)
		for(int i=0; i<mainApp.getArr_configs().get(MainApp.currentConfigID).getArr_Sensors().size(); i++){
			int state =  arr_ToggleButtons.get(i).isSelected()? 1 : 0;			
			mainApp.getArr_configs().get(MainApp.currentConfigID).getArr_Sensors().get(i).setState(state);
		}		
		Utils.updateZone(MainApp.currentConfigID);
		button_saveConfig.setDisable(true);

	}

	@FXML
	private void handleEditZone(){


		mainApp.controller_editZone.updateTextFields();
		mainApp.showEditZoneMenu();
	}

	@FXML
	private void handleSetAll() {
		System.out.println("#handleButton: All Sensor settings");
		mainApp.showSensorSettings(0);
	}

	@FXML
	private void handleSetS1() {
		System.out.println("#handleButton: Sensor 1 settings");
		mainApp.showSensorSettings(1);
	}

	@FXML
	private void handleSetS2() {
		System.out.println("#handleButton: Sensor2 settings");
		mainApp.showSensorSettings(2);
	}

	@FXML
	private void handleSetS3() {
		System.out.println("#handleButton: Sensor3 settings");
		mainApp.showSensorSettings(3);
	}

	@FXML
	private void handleSetS4() {
		System.out.println("#handleButton: Sensor4 settings");
		mainApp.showSensorSettings(4);
	}

	@FXML
	private void handleSetS5() {
		System.out.println("#handleButton: Sensor5 settings");
		mainApp.showSensorSettings(5);
	}

	@FXML
	private void handleSetS6() {
		System.out.println("#handleButton: Sensor6 settings");
		mainApp.showSensorSettings(6);
	}

	@FXML
	private void handleSetS7() {
		System.out.println("#handleButton: Sensor7 settings");
		mainApp.showSensorSettings(7);
	}

	@FXML
	private void handleSetS8() {
		System.out.println("#handleButton: Sensor8 settings");
		mainApp.showSensorSettings(8);
	}

	@FXML
	private void handleGoBack() {
		System.out.println("#handleButton: GoBack");
		mainApp.showMainMenu();
	}

	@FXML
	private void handleSelection() {
		button_saveConfig.setDisable(true);
		int selectedIndex = cBox_Settings.getSelectionModel().getSelectedIndex();
		System.out.println("#handleSelection: selected index: "+selectedIndex);     	
		if(selectionCounter!=0){
			MainApp.currentConfigID = selectedIndex;
			System.out.println("\tCurrentConfigID value changed to "+MainApp.currentConfigID);
			mainApp.initConfiguration();
		}
		selectionCounter=1;
	}

	@FXML
	private void handleToggleButton() {
		button_saveConfig.setDisable(false);
	}

	@FXML
	private void handleSetMode(){
		if(mode == 0){
			setMode(1);
		}else if(mode == 1){
			setMode(0);
		}else{
			System.err.println("Wrong mode. Setting mode to 1");
			setMode(1);
		}
	}


	private void setMode(int mode) {
		switch(mode){
		case 0:
			button_mode.setText("Normal_Close");
			this.mode=0;
			MainApp.port_com.writeLine("6:0:0:0:0:0:0:0:0:\r\n");
			break;
		case 1:
			button_mode.setText("Normal_Open");
			this.mode=1;
			MainApp.port_com.writeLine("6:1:1:1:1:1:1:1:1:\r\n");
			break;
		}
	}

	public void sendMode() {
		if(mode==1){
			MainApp.port_com.writeLine("6:1:1:1:1:1:1:1:1:\r\n");
		}else if(mode==0){
			MainApp.port_com.writeLine("6:0:0:0:0:0:0:0:0:\r\n");
		}
		else{			
			System.err.println("Wrong mode. Setting mode to 1");
			setMode(1);
		}
	}

	public void disableSensor(int buttonIndex) {
		switch(buttonIndex){
		case 0: button_setS1.setDisable(true);
		case 1: button_setS2.setDisable(true);
		case 2: button_setS3.setDisable(true);
		case 3: button_setS4.setDisable(true);
		case 4: button_setS5.setDisable(true);
		case 5: button_setS6.setDisable(true);
		case 6: button_setS7.setDisable(true);
		case 7: button_setS8.setDisable(true);
		}

	}

	public void setSensorButtonName(int index, String buttonName) {
		switch(index){
		case 0: button_setS1.setText(buttonName);
		break;
		case 1: button_setS2.setText(buttonName);
		break;
		case 2: button_setS3.setText(buttonName);
		break;
		case 3: button_setS4.setText(buttonName);
		break;
		case 4: button_setS5.setText(buttonName);
		break;
		case 5: button_setS6.setText(buttonName);
		break;
		case 6: button_setS7.setText(buttonName);
		break;
		case 7: button_setS8.setText(buttonName);
		break;
		}
	}

	public void setSensorState(int index, int sensorState) {
		if(sensorState==0){
			switch(index){
			case 0: tbutton_S1.setSelected(false);
			break;
			case 1: tbutton_S2.setSelected(false);
			break;
			case 2: tbutton_S3.setSelected(false);
			break;
			case 3: tbutton_S4.setSelected(false);
			break;
			case 4: tbutton_S5.setSelected(false);
			break;
			case 5: tbutton_S6.setSelected(false);
			break;
			case 6: tbutton_S7.setSelected(false);
			break;
			case 7: tbutton_S8.setSelected(false);
			break;
			}
		}else if(sensorState==1){
			switch(index){
			case 0: tbutton_S1.setSelected(true);
			break;
			case 1: tbutton_S2.setSelected(true);
			break;
			case 2: tbutton_S3.setSelected(true);
			break;
			case 3: tbutton_S4.setSelected(true);
			break;
			case 4: tbutton_S5.setSelected(true);
			break;
			case 5: tbutton_S6.setSelected(true);
			break;
			case 6: tbutton_S7.setSelected(true);
			break;
			case 7: tbutton_S8.setSelected(true);
			break;
			}
		}
	}

	public void selectCBIndex(int cbIndex){
		cBox_Settings.getSelectionModel().select(cbIndex);
	}

	public void refreshComboBox() {
		cBox_Settings.setItems(MainApp.oList_configNames);
		cBox_Settings.getSelectionModel().select(MainApp.oList_configNames.size()-1);
	}






}
