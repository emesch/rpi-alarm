package pl.edu.utp.emesch.alarm.view;

import java.util.ArrayList;

import pl.edu.utp.emesch.alarm.MainApp;
import pl.edu.utp.emesch.alarm.Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;

public class ZoneEditController {

	@FXML
	private Button button_goBack;
	@FXML
	private Button button_save;
	@FXML
	private TextField label_zoneName;
	@FXML
	private TextField label_sensor0;
	@FXML
	private TextField label_sensor1;
	@FXML
	private TextField label_sensor2;
	@FXML
	private TextField label_sensor3;
	@FXML
	private TextField label_sensor4;
	@FXML
	private TextField label_sensor5;
	@FXML
	private TextField label_sensor6;
	@FXML
	private TextField label_sensor7;

	//Reference to the main application
	private MainApp mainApp;

	
	/**
	 * The constructor.
	 * The constructor is called before the initialize() method.
	 */
	public ZoneEditController() {    	 
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */

	@FXML
	private void initialize() {	
		button_save.setDisable(true);
		
		label_zoneName.textProperty().addListener((observable, oldValue, newValue) -> {
			button_save.setDisable(false);
		});
		
		label_sensor0.textProperty().addListener((observable, oldValue, newValue) -> {
			button_save.setDisable(false);
		});
		label_sensor1.textProperty().addListener((observable, oldValue, newValue) -> {
			button_save.setDisable(false);
		});
		label_sensor2.textProperty().addListener((observable, oldValue, newValue) -> {
			button_save.setDisable(false);
		});
		label_sensor3.textProperty().addListener((observable, oldValue, newValue) -> {
			button_save.setDisable(false);
		});
		label_sensor4.textProperty().addListener((observable, oldValue, newValue) -> {
			button_save.setDisable(false);
		});
		label_sensor5.textProperty().addListener((observable, oldValue, newValue) -> {
			button_save.setDisable(false);
		});
		label_sensor6.textProperty().addListener((observable, oldValue, newValue) -> {
			button_save.setDisable(false);
		});
		label_sensor7.textProperty().addListener((observable, oldValue, newValue) -> {
			button_save.setDisable(false);
		});
		
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;        
	}

	@FXML
	private void handleSave(){
		ArrayList<String> textFieldValues = new ArrayList<>();
		textFieldValues.add(label_zoneName.getText());
		textFieldValues.add(label_sensor0.getText());
		textFieldValues.add(label_sensor1.getText());
		textFieldValues.add(label_sensor2.getText());
		textFieldValues.add(label_sensor3.getText());
		textFieldValues.add(label_sensor4.getText());
		textFieldValues.add(label_sensor5.getText());
		textFieldValues.add(label_sensor6.getText());
		textFieldValues.add(label_sensor7.getText());
		
		Utils.updateZoneInfo(textFieldValues);		
	}
	
	@FXML
	private void handle_goBack(){
		button_save.setDisable(true);
		mainApp.showSettingsMenu();
	}
	
	@FXML
	private void handle_textChange(){
		button_save.setDisable(false);
	}

	public void updateTextFields() {
		String zoneName = MainApp.arr_configs.get(MainApp.currentConfigID).getName();
		ArrayList<String> sensorNames = new ArrayList<>();
		for(int i=0; i<MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().size(); i++){
			sensorNames.add(MainApp.arr_configs.get(MainApp.currentConfigID).getArr_Sensors().get(i).getName());
		}
		
		label_zoneName.setText(zoneName);
		label_sensor0.setText(sensorNames.get(0));
		label_sensor1.setText(sensorNames.get(1));
		label_sensor2.setText(sensorNames.get(2));
		label_sensor3.setText(sensorNames.get(3));
		label_sensor4.setText(sensorNames.get(4));
		label_sensor5.setText(sensorNames.get(5));
		label_sensor6.setText(sensorNames.get(6));
		label_sensor7.setText(sensorNames.get(7));
		
		button_save.setDisable(true);
	}
	
	






}
