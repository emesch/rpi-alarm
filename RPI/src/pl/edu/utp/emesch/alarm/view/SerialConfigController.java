package pl.edu.utp.emesch.alarm.view;

import gnu.io.PortInUseException;

import java.util.List;

import pl.edu.utp.emesch.alarm.MainApp;
import pl.edu.utp.emesch.alarm.Utils;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

public class SerialConfigController {

	@FXML
	private ComboBox<String> cb_serialSelection;
	@FXML
	private Button button_ok;
	@FXML
	private Button button_test;
	@FXML
	private Label status;

	private String selectedPort;

	//Reference to the main application
	private MainApp mainApp;
	/**
	 * True, when waiting for avr response. Used in waitForResponse(). 
	 */
	private boolean isWaitingForResponse;
	/**
	 * -1 - default
	 *  0 - timeout,
	 *  1 - ready
	 */
	private int statusID;	

	/**
	 * The constructor.
	 * The constructor is called before the initialize() method.
	 */
	public SerialConfigController() {
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */

	@FXML
	private void initialize() {
		//cb_serialSelection.setItems(MainApp.oList_configNames);
		button_test.setDefaultButton(true);
		button_ok.setDisable(true);
		setWaitingForResponse(false);
		statusID = -1;
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;        
	}

	@FXML
	private void handleOK() {		
		MainApp.thr_alarmContr.start();
		MainApp.showMainMenu();
	}

	@FXML
	private void handleTest() {
		if(cb_serialSelection.getSelectionModel().getSelectedIndex()!=-1){
			button_test.setDisable(true);
			cb_serialSelection.setDisable(true);
			if(MainApp.isConnected()){
				MainApp.port_com.disconnect();			
				MainApp.setConnected(false);
			}
			try{			
				MainApp.port_com.connect(getSelectedPortName());
				MainApp.setConnected(true);
				MainApp.port_com.initListener();
				MainApp.port_com.initIOStream();
			}catch(PortInUseException e){			
				status.setText("Ten port jest zajety!");
				MainApp.setConnected(false);				
				cb_serialSelection.setDisable(false);
			}
			if(MainApp.isConnected()){
				status.setText("Polaczono...");
				MainApp.port_com.writeLine("8:\r\n");
				setWaitingForResponse(true);
				waitForResponse();
			}
		}
	}

	private void waitForResponse() {		
		status.setText("Oczekiwanie...");
		button_test.setDisable(true);		

		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {	
				int counter = 0;
				while(isWaitingForResponse() && counter <100){
					try {
						Thread.sleep(100);
						counter++;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if(counter == 100){
					statusID = 0;
					MainApp.port_com.disconnect();
				}else{
					statusID = 1;
				}				

				Platform.runLater(new Runnable(){

					@Override
					public void run() {						
						switch(statusID){
						case 0: 
							status.setText("Brak odpowiedzi");
							break;
						case 1:
							status.setText("OK");
							button_ok.setDisable(false);
						}
						statusID = -1;
						cb_serialSelection.setDisable(false);
					}
					
				});
				//setWaitingForResponse(false);
			}

		});		

		thread.start();
	}














	@FXML
	private void handleSelection(){		
		int selectedIndex = cb_serialSelection.getSelectionModel().getSelectedIndex();
		setSelectedPort(cb_serialSelection.getValue());
		status.setText("");
		if(MainApp.isConnected()){
			MainApp.port_com.disconnect();
			MainApp.setConnected(false);
		}
		if(button_test.isDisable()){
			button_test.setDisable(false);
		}

	}

	private void setSelectedPort(String portName) {
		this.selectedPort = portName;
	}

	public String getSelectedPortName(){
		return selectedPort;
	}

	public void loadPortList() {		
		cb_serialSelection.setItems(MainApp.oList_portNames);		
	}

	public boolean isWaitingForResponse() {
		return isWaitingForResponse;
	}

	public void setWaitingForResponse(boolean isWaitingForResponse) {
		System.out.println("SerialConfig: Waiting for response - "+isWaitingForResponse);
		this.isWaitingForResponse = isWaitingForResponse;
	}

}
