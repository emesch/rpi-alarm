package pl.edu.utp.emesch.alarm.view;

import pl.edu.utp.emesch.alarm.MainApp;
import javafx.application.Platform;
import javafx.beans.binding.SetBinding;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

public class ErrorMenuController {
	
	@FXML
	private TextArea tArea_errorMSG;
	@FXML
	private Button button_settings;
	
	
	//Reference to the main application
	private MainApp mainApp;
	
	 /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public ErrorMenuController() {
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    
    @FXML
    private void initialize() {    	
    }
    
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */    
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;        
    }
    
          
    @FXML
    private void handleSettings() {
    	System.out.println("#handleButton: Settings");
    	mainApp.showSettingsMenu();
    }

	public String getErrorMSG() {
		return tArea_errorMSG.getText();
	}

	public void setErrorMSG(String status) {
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				tArea_errorMSG.setText(status);				
			}
		});		
	}
	
	
	
	

}
