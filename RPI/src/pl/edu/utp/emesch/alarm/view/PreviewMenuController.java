package pl.edu.utp.emesch.alarm.view;

import pl.edu.utp.emesch.alarm.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class PreviewMenuController {
	
	@FXML
	private Button button_reset;
	@FXML
	private Button button_details;
	@FXML
	private Button button_back;
	
	@FXML
	private Label label_zone;
	@FXML
	private Label label_sensorCount;
	@FXML
	private Label label_eventCount;
	@FXML
	private Label label_lastEvent;
	
	
	
	//Reference to the main application
	private MainApp mainApp;
	
	 /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public PreviewMenuController() {
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    
    @FXML
    private void initialize() {
    	label_zone.setText("-");
    	label_eventCount.setText("-");
    	label_lastEvent.setText("-");
    	label_sensorCount.setText("-");
    }
    
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;        
    }
    
    @FXML
    private void handleDetails(){
    	System.out.println("#handleButton: handleDetails");
    	mainApp.showDetailsMenu();
    }
    
    @FXML
    private void handleGoBack() {
    	System.out.println("#handleButton: GoBack");
    	mainApp.showMainMenu();
    }
    
    public void setZoneName(String zoneName) {
    	label_zone.setText(zoneName);
    }
    
    public void setSensorCount(int sensorCount) {    	
    	label_sensorCount.setText(Integer.toString(sensorCount));
    }
    
    
}
