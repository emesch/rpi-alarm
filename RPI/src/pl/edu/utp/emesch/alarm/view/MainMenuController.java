package pl.edu.utp.emesch.alarm.view;

import pl.edu.utp.emesch.alarm.MainApp;
import javafx.application.Platform;
import javafx.beans.binding.SetBinding;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class MainMenuController {
	
	@FXML
	private Button button_activate;
	@FXML
	private Button button_preview;
	@FXML
	private Button button_settings;
	
	@FXML
	private Label label_status;
	@FXML
	private Label label_zone;
	@FXML
	private Label label_events;
	@FXML
	private Label label_eventCount;
	
	//Reference to the main application
	private MainApp mainApp;
	
	 /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public MainMenuController() {
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    
    @FXML
    private void initialize() {    	
    }
    
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;        
    }
    
    @FXML
    private void handleActivateAlarm() {
    	System.out.println("#handleButton: Activate Alarm");
    	mainApp.showActivateMenu();
    }
    
    @FXML
    private void handlePreview() {
    	System.out.println("#handleButton: Preview");
    	mainApp.showPreviewMenu();
    }
    
    @FXML
    private void handleSettings() {
    	System.out.println("#handleButton: Settings");
    	mainApp.showSettingsMenu();
    }

	public String getStatus() {
		return label_status.getText();
	}

	public void setStatus(String status) {
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				label_status.setText(status);				
			}
		});		
	}
	
	public void setArmed(){
		button_activate.setText("ROZBROJENIE");
	}
	
	public void setDisarmed(){
		button_activate.setText("UZBROJENIE");
	}

	public void setZone(String configName) {
		label_zone.setText(configName);		
	}
	
	public void setDisableActivateButton(boolean disable){
		Platform.runLater(new Runnable(){
			@Override
			public void run() {
				button_activate.setDisable(disable);
			}			
		});
		
	}
	
	

}
