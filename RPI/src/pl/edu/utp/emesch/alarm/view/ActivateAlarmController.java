package pl.edu.utp.emesch.alarm.view;

import pl.edu.utp.emesch.alarm.AlarmController;
import pl.edu.utp.emesch.alarm.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;

public class ActivateAlarmController {

	@FXML
	private Button button1;
	@FXML
	private Button button2;
	@FXML
	private Button button3;
	@FXML
	private Button button4;
	@FXML
	private Button button5;
	@FXML
	private Button button6;
	@FXML
	private Button button7;
	@FXML
	private Button button8;
	@FXML
	private Button button9;
	@FXML
	private Button button0;
	@FXML
	private Button button_Clear;
	@FXML
	private Button button_Back;
	@FXML
	private Button button_OK;
	@FXML
	private Button button_Settings;
	@FXML
	private Label label_title;

	@FXML
	private Label label_whiteScreen;
	private String s_whiteScreen;
	private String s_enteredCode;

	//Reference to the main application
	private MainApp mainApp;


	/**
	 * The constructor.
	 * The constructor is called before the initialize() method.
	 */
	public ActivateAlarmController() {
		s_whiteScreen = "";
		s_enteredCode = "";

	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */

	@FXML
	private void initialize() {		   	
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;        
	}

	@FXML
	private void handleKeyboard0() {
		System.out.println("#handleButton: handleKeyboard0");		
		s_whiteScreen += "*";
		s_enteredCode += "0";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleKeyboard1() {
		System.out.println("#handleButton: handleKeyboard1");		
		s_whiteScreen += "*";
		s_enteredCode += "1";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleKeyboard2() {
		System.out.println("#handleButton: handleKeyboard2");		
		s_whiteScreen += "*";
		s_enteredCode += "2";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleKeyboard3() {
		System.out.println("#handleButton: handleKeyboard3");		
		s_whiteScreen += "*";
		s_enteredCode += "3";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleKeyboard4() {
		System.out.println("#handleButton: handleKeyboard4");		
		s_whiteScreen += "*";
		s_enteredCode += "4";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleKeyboard5() {
		System.out.println("#handleButton: handleKeyboard5");		
		s_whiteScreen += "*";
		s_enteredCode += "5";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleKeyboard6() {
		System.out.println("#handleButton: handleKeyboard6");		
		s_whiteScreen += "*";
		s_enteredCode += "6";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleKeyboard7() {
		System.out.println("#handleButton: handleKeyboard7");		
		s_whiteScreen += "*";
		s_enteredCode += "7";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleKeyboard8() {
		System.out.println("#handleButton: handleKeyboard8");		
		s_whiteScreen += "*";
		s_enteredCode += "8";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleKeyboard9() {
		System.out.println("#handleButton: handleKeyboard9");		
		s_whiteScreen += "*";
		s_enteredCode += "9";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleClear() {
		System.out.println("#handleButton: Clear");
		s_whiteScreen = "";
		s_enteredCode = "";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleGoBack() {
		System.out.println("#handleButton: GoBack");
		mainApp.showMainMenu();
		s_whiteScreen = "";
		s_enteredCode = "";
		label_whiteScreen.setText(s_whiteScreen);
		System.out.println("-WHITE SCREEN CLEARED-");
		System.out.println("?whiteScreen: "+s_whiteScreen);
		System.out.println("?enteredCode: "+s_enteredCode);
	}

	@FXML
	private void handleActivateAlarm() {
		String correctCode = mainApp.getAlarmCode();
		if(!MainApp.isArmed()){
			if(s_enteredCode.equals(correctCode)){
				s_whiteScreen = "";
				s_enteredCode = "";
				label_whiteScreen.setText(s_whiteScreen);				
				mainApp.showMainMenu();
				MainApp.alarmController.arm();
				MainApp.alarmController.setCodeChances(2);
			}else{
				System.out.println("!!WRONG CODE!!");
			}
			//else if is Forced	
		}else {
			if(s_enteredCode.equals(correctCode)){
				s_whiteScreen = "";
				s_enteredCode = "";
				label_whiteScreen.setText(s_whiteScreen);
				MainApp.alarmController.disarm();
			}else{
				System.out.println("!!WRONG CODE!!");
				if(MainApp.alarmController.getCodeChances()>0){
					MainApp.setWrongCodeStyle();
					MainApp.alarmController.setCodeChances(MainApp.alarmController.getCodeChances()-1);
					System.out.println(MainApp.alarmController.getCodeChances()+" chances left.");
				}else {
					MainApp.alarmController.turnOnTheAlarm();
					mainApp.showMainMenu();
				}
			}
		}

	}

	public void setArmed(){
		label_title.setText("ROZBROJENIE ALARMU");
	}

	public void setDisarmed(){
		label_title.setText("AKTYWACJA ALARMU");
	}





}
