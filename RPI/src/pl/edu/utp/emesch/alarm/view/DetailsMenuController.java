package pl.edu.utp.emesch.alarm.view;

import java.util.ArrayList;

import pl.edu.utp.emesch.alarm.MainApp;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class DetailsMenuController {

	@FXML
	private Button button_back;
	@FXML
	private Label label_sensor1;
	@FXML
	private Label label_sensor2;
	@FXML
	private Label label_sensor3;
	@FXML
	private Label label_sensor4;
	@FXML
	private Label label_sensor5;
	@FXML
	private Label label_sensor6;
	@FXML
	private Label label_sensor7;
	@FXML
	private Label label_sensor8;
	@FXML
	private Label label_sensor1Status;
	@FXML
	private Label label_sensor2Status;
	@FXML
	private Label label_sensor3Status;
	@FXML
	private Label label_sensor4Status;
	@FXML
	private Label label_sensor5Status;
	@FXML
	private Label label_sensor6Status;
	@FXML
	private Label label_sensor7Status;
	@FXML
	private Label label_sensor8Status;
	
	private ArrayList<Label> arr_sensorLabels;


	//Reference to the main application
	private MainApp mainApp;

	/**
	 * The constructor.
	 * The constructor is called before the initialize() method.
	 */
	public DetailsMenuController() {		
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */

	@FXML
	private void initialize() {
		arr_sensorLabels = new ArrayList<>();
		arr_sensorLabels.add(label_sensor1);
		arr_sensorLabels.add(label_sensor2);
		arr_sensorLabels.add(label_sensor3);
		arr_sensorLabels.add(label_sensor4);
		arr_sensorLabels.add(label_sensor5);
		arr_sensorLabels.add(label_sensor6);
		arr_sensorLabels.add(label_sensor7);
		arr_sensorLabels.add(label_sensor8);
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;        
	}

	@FXML
	private void handleGoBack() {
		System.out.println("#handleButton: GoBack");
		mainApp.showMainMenu();
	}

	public void updateDetails(int sensorID, String status){
		//System.err.println("!!UpdateDetails: "+sensorID + " | "+status);
		Platform.runLater(new Runnable(){

			@Override
			public void run() {
				switch(sensorID){
				case 0:
					label_sensor1Status.setText(status);
					break;
				case 1:
					label_sensor2Status.setText(status);
					break;
				case 2:
					label_sensor3Status.setText(status);
					break;
				case 3:
					label_sensor4Status.setText(status);
					break;
				case 4:
					label_sensor5Status.setText(status);
					break;
				case 5:
					label_sensor6Status.setText(status);
					break;
				case 6:
					label_sensor7Status.setText(status);
					break;
				case 7:
					label_sensor8Status.setText(status);
					break;
				}
			}

		});

	}

	@FXML
	private void handle_goBack(){
		System.out.println("#handleButton: GoBack");
		mainApp.showPreviewMenu();
	}

	public void setSensorLabelName(int i, String sensorName) {
		arr_sensorLabels.get(i).setText(sensorName);		
	}


}
