package pl.edu.utp.emesch.alarm.view;

import pl.edu.utp.emesch.alarm.MainApp;
import pl.edu.utp.emesch.alarm.Utils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;

public class SensorSettingsMenuController{
	
	@FXML
	private Label label_settingsName;
	@FXML
	private Label label_response;
	@FXML
	private Label label_keeptime;
	@FXML
	private ScrollBar scr_response;
	@FXML
	private ScrollBar scr_keeptime;
	@FXML
	private Button button_goBack;
	@FXML
	private Button button_save;
	
	/**
	 * if 0 - Settings for all sensors
	 * if 1-8 - Settings for actual sensor
	 */
	private int controllerID;
	
	
	
	//Reference to the main application
	private MainApp mainApp;
	
	 /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public SensorSettingsMenuController() {
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    
    @FXML
    private void initialize() {
    	button_save.setDisable(true);
    }
    
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;        
    }
    
        
    @FXML
    private void handleGoBack() {
    	System.out.println("#handleButton: GoBack");
    	mainApp.showSettingsMenu();
    }

	public int getResponseTime() {
		return Integer.parseInt(label_response.getText());
	}
	
	public void setResponseTime(int responseTime) {		
		label_response.setText(Integer.valueOf(responseTime).toString());		
	}

	public int getKeepTime() {		
		return Integer.parseInt(label_keeptime.getText());
	}
	
	public void setKeepTime(int keepTime) {		
		label_keeptime.setText(Integer.valueOf(keepTime).toString());		
	}
	
	public void setSettingsName(String settingsName) {
		label_settingsName.setText(settingsName);
	}
	
	public void setScrollBarValue(int scrollBarID, double value){
		switch(scrollBarID){
		case 0:
			scr_response.setValue(value);
			break;
		case 1:
			scr_keeptime.setValue(value);
			break;
		}
	}
	
	@FXML
	private void handleResponseScroll() {
		Double value = (double) Math.round(scr_response.getValue());
		label_response.setText(Integer.toString(value.intValue()));
		button_save.setDisable(false);
	}
	
	@FXML
	private void handleKeeptimeScroll() {
		Double value = (double) Math.round(scr_keeptime.getValue());
		label_keeptime.setText(Integer.toString(value.intValue()));
		button_save.setDisable(false);
	}

	public int getControllerID() {
		return controllerID;
	}

	public void setControllerID(int controllerID) {
		this.controllerID = controllerID;
	}
	
	@FXML
    public void handleSave(){		
		if(controllerID == 0){
			Utils.saveSensorSettings4All();
		}else if(controllerID>0 && controllerID<=8){
			Utils.saveSensorSettings(controllerID);
		}
	}

	public void setDisableSaveButton(boolean disable) {
		button_save.setDisable(disable);		
	}
    
}
