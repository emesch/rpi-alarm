package pl.edu.utp.emesch.alarm.serial;

import gnu.io.PortInUseException;

public class SerialControllerTest {

	public static SerialController testCom;
	/**
	 * Count checking sensors. 
	 */
	public static int counter = 0;

	public static void main(String[] args) {
		testCom = new SerialController();
		String[] ports = testCom.searchForPorts();
		try {
			testCom.connect("COM9");
		} catch (PortInUseException e) {		
			System.err.println("PORT IN USE!!");
		}	    
		testCom.initListener();
		testCom.initIOStream();
		System.out.println("\n\n\n");
		//test.writeLine("1^2");

	}

	public static void emulateResponse(int commandID) {
		switch(commandID){
		//sending 'OK' to CENTRAL
		case 5:
			testCom.writeLine("0:");
			break;
			//sending sensor state
		case 7:
			counter++;
			if(counter<20){
				testCom.writeLine("1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:");
				break;
			}else if(counter<40){
				testCom.writeLine("1:0:0:0:1:0:0:0:0:0:0:0:0:0:0:0:0:");
				break;
			}else{
				testCom.writeLine("1:0:0:0:1:0:0:0:0:0:0:0:0:0:0:0:0:");
				break;
			}
		case 8:
			testCom.writeLine("2:");
			break;
		}
	}

}


