package pl.edu.utp.emesch.alarm.serial;

import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.TooManyListenersException;

import pl.edu.utp.emesch.alarm.MainApp;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

public class SerialController implements SerialPortEventListener {
	private Enumeration ports = null;
	//map the port names to CommPortIdentifiers
	private HashMap portMap = new HashMap();

	//this is the object that contains the opened port
	private CommPortIdentifier selectedPortIdentifier = null;
	private SerialPort serialPort = null;

	//input and output streams for sending and receiving data
	private InputStream input = null;
	private OutputStream output = null;

	//just a boolean flag that i use for enabling
	//and disabling buttons depending on whether the program
	//is connected to a serial port or not
	private boolean bConnected = false;

	//the timeout value for connecting with the port
	final static int TIMEOUT = 2000;

	//some ascii values for certain things
	final static int SPACE_ASCII = 32;
	final static int DASH_ASCII = 45;
	final static int NEW_LINE_ASCII = 10;

	//a string for recording what goes on in the program
	//this string is written to the GUI
	String logText = "";
	private String[] tokens;

	//private boolean eventFlag = false;

	@Override
	public void serialEvent(SerialPortEvent arg0) {
		System.out.println("\n\n");
		System.out.println("*********************************************");
		//eventFlag = true;
		//System.out.println("$SerialController#serialEvent | eventFlag=true");
		try {
			String rx = read_line();
			System.out.println("-----------------------------------------");
			System.out.println(rx);
			System.out.println("-----------------------------------------");			
			tokens = rx.split(":");			
			processMSG();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		//System.out.println("$SerialController#serialEvent | eventFlag=false");
		System.out.println("*********************************************");
		System.out.println("\n\n");
		//eventFlag = false;
	}

	private void processMSG() {		
		//First token is a unique commandID
		int commandID = Integer.valueOf(tokens[0]);
		String[] tokens0 = tokens;
		//System.out.println("#processMSG-commandID: "+commandID);
		switch(commandID){
		//AVR - CENTRAL
		case 0:
			process0_receiveOK();
			break;
		case 1:
			//System.err.println("#processMSG-CASE1: tokens size: "+tokens.length+" | tokens0 size: "+tokens0.length);
			process1_receiveSensorState(tokens0);
			break;
		case 2:
			//System.err.println("#processMSG-CASE1: tokens size: "+tokens.length+" | tokens0 size: "+tokens0.length);
			process2_receiveTestOK(tokens0);
		case 3:
			process3_resetOccured();
			break;
			
			//CENTRAL-AVR
		case 5:
			process5_setSensors();
			break;
		case 7:
			process7_set();
			break;
		case 8:
			process8_sendOK();
			break;
		}
		

	}	

	/**
	 * 'OK' message received from AVR to confirm sensor setting. 
	 */
	private void process0_receiveOK() {
		//System.out.println("Data to process: "+(tokens.length-1));
		MainApp.alarmController.setAVRConfigured(true);
	}

	/**
	 * 'Sensor State Message' received from AVR. 
	 * @param tokens0 
	 */
	private void process1_receiveSensorState(String[] tokens0) {		
		//System.out.println("[PROCESS1]Data to process: "+(tokens0.length-1));		
		MainApp.alarmController.updateSensorState(tokens0);	
		//MainApp.alarmController.setWaitingForResponse(false);
	}
	
	private void process2_receiveTestOK(String[] tokens0) {
		//System.out.println("[PROCESS1]Data to process: "+(tokens0.length-1));
		MainApp.controller_serialConfig.setWaitingForResponse(false);
	}

	private void process3_resetOccured() {
		System.err.println("AVR RESET OCCURED! Sending Config Data...");
		MainApp.alarmController.sendConfigData();		
	}

	/**
	 * 'Change sensorSettings request'.
	 */
	private void process5_setSensors() {
		//System.out.println("Data to process: "+(tokens.length-1));
		SerialControllerTest.emulateResponse(5);
	}

	/**
	 * 'Change input register mode request'. Set mode: Analog/Digital.
	 */
	private void process6_setMode() {
		//System.out.println("Data to process: "+(tokens.length-1));
	}

	/**
	 * 'Receive state request'. 
	 */
	private void process7_set() {
		//System.out.println("Data to process: "+(tokens.length-1));
		SerialControllerTest.emulateResponse(7);
	}
	
	private void process8_sendOK() {
		//System.out.println("Data to process: "+(tokens.length-1));
		SerialControllerTest.emulateResponse(8);		
	}

	private String read_line() throws IOException {
		System.out.println("||READ_LINE||");
		int data = 0;
		int len = 0;
		int availableBytes = input.available();
		byte[] bufor = new byte[1024];
		String rx = null;

		try{
			if (availableBytes > 0) {
				do{
					data =  input.read();
					if((char)data=='\n'){
						break;
					}
					bufor[len++] = (byte)data;
				}while(data!=-1);
			}
		}catch (Exception e){
			logText = "Failed to read data. (" + e.toString() + ")";
			System.out.println(logText);
		}
		return new String(bufor,0,len);
	}


	public String[] searchForPorts(){
		System.out.println("$SerialController#searchForPorts");
		ports = CommPortIdentifier.getPortIdentifiers();
		int i = 0;
		ArrayList<String> porty = new ArrayList<String>();
		while (ports.hasMoreElements()){
			CommPortIdentifier curPort = (CommPortIdentifier)ports.nextElement();
			//get only serial ports
			if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL){
				portMap.put(curPort.getName(), curPort);
				porty.add(curPort.getName());
				//System.out.println(curPort.getName());
			}
		}
		String[] lista = porty.toArray(new String[0]);
		return lista;
	}

	public void connect(String port) throws PortInUseException{
		System.out.println("$SerialController#connect");
		String selectedPort = port;
		selectedPortIdentifier = (CommPortIdentifier)portMap.get(selectedPort);
		CommPort commPort = null;


		//the method below returns an object of type CommPort
		commPort = selectedPortIdentifier.open("TigerControlPanel", TIMEOUT);
		//the CommPort object can be casted to a SerialPort object
		serialPort = (SerialPort)commPort;
		//for controlling GUI elements
		//logging
		logText = selectedPort + " opened successfully.";
		System.out.println("\t$SerialController: "+logText);

	}

	public boolean initIOStream(){
		System.out.println("$SerialController#initIOStream");
		//return value for whether opening the streams is successful or not
		boolean successful = false;
		try {
			//
			input = serialPort.getInputStream();
			output = serialPort.getOutputStream();

			successful = true;
			return successful;
		}
		catch (IOException e) {
			logText = "I/O Streams failed to open. (" + e.toString() + ")";
			System.out.println(logText);
			return successful;
		}
	}


	public void disconnect(){
		System.out.println("$SerialController#disconnect");
		//close the serial port
		try{
			serialPort.removeEventListener();
			serialPort.close();
			input.close();
			output.close();
			logText = "Disconnected.";
			System.out.println(logText);
		}
		catch (Exception e)
		{
			logText = "Failed to close " + serialPort.getName()
					+ "(" + e.toString() + ")";
			System.out.println(logText);
		}
	}

	public void initListener(){
		System.out.println("$SerialController#initListener");
		try
		{
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
		}
		catch (TooManyListenersException e)
		{
			logText = "Too many listeners. (" + e.toString() + ")";
			System.out.println(logText);

		}
	}



	public void read_data() throws IOException{
		System.out.println("$SerialController#read_data");
		int data = 0;
		int len = 0;
		int availableBytes = input.available();
		byte[] bufor = new byte[1024];
		String rx = null;

		try
		{
			if (availableBytes > 0) {
				do{
					data =  input.read();
					if((char)data=='\n'){
						break;
					}
					bufor[len++] = (byte)data;
				}while(data!=-1);
				System.out.println(new String(bufor,0,len));
			}
		}
		catch (Exception e){
			logText = "Failed to read data. (" + e.toString() + ")";
			System.out.println(logText);
		}
	}

	public void writeLine(String data){
		System.out.println("\n");
		System.out.println("||SENDING DATA||");
		System.out.println(">>>>>>>>>>>");
		System.out.println(data);
		System.out.println(">>>>>>>>>>>");
		System.out.println("\n");
		try {
			output.write(data.getBytes());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		try {
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	//	public boolean getEventFlag() {
	//		return eventFlag;
	//	}


}
