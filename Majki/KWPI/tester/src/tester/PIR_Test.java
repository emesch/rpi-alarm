package tester;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.List;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.TooManyListenersException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

import java.awt.FlowLayout;

import javax.swing.border.BevelBorder;
import javax.swing.JButton;

import org.omg.CORBA.PRIVATE_MEMBER;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import java.awt.Font;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.SwingConstants;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JSpinner;
import javax.swing.JTextPane;
import javax.swing.JTextArea;


abstract class Communicator implements SerialPortEventListener
{
	private Enumeration ports = null;
	//map the port names to CommPortIdentifiers
	private HashMap portMap = new HashMap();

	//this is the object that contains the opened port
	private CommPortIdentifier selectedPortIdentifier = null;
	private SerialPort serialPort = null;

	//input and output streams for sending and receiving data
	private InputStream input = null;
	private OutputStream output = null;

	//just a boolean flag that i use for enabling
	//and disabling buttons depending on whether the program
	//is connected to a serial port or not
	private boolean bConnected = false;

	//the timeout value for connecting with the port
	final static int TIMEOUT = 2000;

	//some ascii values for for certain things
	final static int SPACE_ASCII = 32;
	final static int DASH_ASCII = 45;
	final static int NEW_LINE_ASCII = 10;

	//a string for recording what goes on in the program
	//this string is written to the GUI
	String logText = "";

	//search for all the serial ports
	//pre style="font-size: 11px;": none
	//post: adds all the found ports to a combo box on the GUI
	public String[] searchForPorts()
	{
		ports = CommPortIdentifier.getPortIdentifiers();
		int i = 0;
		ArrayList<String> porty = new ArrayList<String>();

		while (ports.hasMoreElements())
		{
			CommPortIdentifier curPort = (CommPortIdentifier)ports.nextElement();

			//get only serial ports
			if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL)
			{

				portMap.put(curPort.getName(), curPort);
				porty.add(curPort.getName());


				//System.out.println(curPort.getName());




			}



		}
		String[] lista = porty.toArray(new String[0]);
		return lista;
	}

	public void connect(String port)
	{
		String selectedPort = port;
		selectedPortIdentifier = (CommPortIdentifier)portMap.get(selectedPort);

		CommPort commPort = null;

		try
		{
			//the method below returns an object of type CommPort
			commPort = selectedPortIdentifier.open("TigerControlPanel", TIMEOUT);
			//the CommPort object can be casted to a SerialPort object
			serialPort = (SerialPort)commPort;

			//for controlling GUI elements


			//logging
			logText = selectedPort + " opened successfully.";
			System.out.println(logText);



		}
		catch (PortInUseException e)
		{
			logText = selectedPort + " is in use. (" + e.toString() + ")";

			System.out.println(logText);


		}
		catch (Exception e)
		{
			logText = "Failed to open " + selectedPort + "(" + e.toString() + ")";
			System.out.println(logText);

		}
	}

	public boolean initIOStream()
	{
		//return value for whether opening the streams is successful or not
		boolean successful = false;

		try {
			//
			input = serialPort.getInputStream();
			output = serialPort.getOutputStream();


			successful = true;
			return successful;
		}
		catch (IOException e) {
			logText = "I/O Streams failed to open. (" + e.toString() + ")";
			System.out.println(logText);
			return successful;
		}
	}






	public void disconnect()
	{
		//close the serial port
		try
		{


			serialPort.removeEventListener();
			serialPort.close();
			input.close();
			output.close();


			logText = "Disconnected.";
			System.out.println(logText);
		}
		catch (Exception e)
		{
			logText = "Failed to close " + serialPort.getName()
					+ "(" + e.toString() + ")";
			System.out.println(logText);
		}
	}

	public void initListener()
	{
		try
		{
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
		}
		catch (TooManyListenersException e)
		{
			logText = "Too many listeners. (" + e.toString() + ")";
			System.out.println(logText);

		}
	}



	public void read_data() throws IOException
	{

		int data = 0;
		int len = 0;
		int availableBytes = input.available();
		byte[] bufor = new byte[1024];
		String rx = null;

		try
		{
			if (availableBytes > 0) {
				

				do
				{
					data =  input.read();
					if((char)data=='\n')
					{
						break;
					}
					bufor[len++] = (byte)data;


				}while(data!=-1);
				System.out.println(new String(bufor,0,len));

			}
		}
		catch (Exception e)
		{
			logText = "Failed to read data. (" + e.toString() + ")";
			System.out.println(logText);
		}




	}
	String read_line() throws IOException
	{
		int data = 0;
		int len = 0;
		int availableBytes = input.available();
		byte[] bufor = new byte[1024];
		String rx = null;

		try
		{
			if (availableBytes > 0) {
				// Read the serial port
				//inStream.read(readBuffer, 0, availableBytes);


				// Print it out

				do
				{
					data =  input.read();
					if((char)data=='\n')
					{
						break;
					}
					bufor[len++] = (byte)data;


				}while(data!=-1);
				//System.out.println(new String(bufor,0,len));

			}
		}
		catch (Exception e)
		{
			logText = "Failed to read data. (" + e.toString() + ")";
			System.out.println(logText);
		}

		return new String(bufor,0,len);

	}
	
	void WriteLine(String data)
	{
		try {
			output.write(data.getBytes());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			output.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}



public class PIR_Test extends JFrame {





	private JPanel contentPane;
	
	public JPanel getContentPane() {
		return contentPane;
	}
	
	
	//variable for received string
	String[] tokens = new String[32]; //string array of tokenised string
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PIR_Test frame = new PIR_Test();
					frame.setVisible(true);
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PIR_Test() {


		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 347, 462);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JPanel[] panels = null;
		

		final JPanel panel = new JPanel();

		panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel.setBounds(49, 26, 47, 21);
		contentPane.add(panel);

		final JPanel panel_1 = new JPanel();
		panel_1.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_1.setBounds(49, 58, 47, 21);
		contentPane.add(panel_1);

		final JPanel panel_2 = new JPanel();
		panel_2.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_2.setBounds(49, 90, 47, 21);
		contentPane.add(panel_2);

		final JPanel panel_3 = new JPanel();
		panel_3.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_3.setBounds(49, 122, 47, 21);
		contentPane.add(panel_3);

		final JPanel panel_4 = new JPanel();
		panel_4.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_4.setBounds(162, 26, 47, 21);
		contentPane.add(panel_4);

		final JPanel panel_5 = new JPanel();
		panel_5.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_5.setBounds(162, 58, 47, 21);
		contentPane.add(panel_5);

		final JPanel panel_6 = new JPanel();
		panel_6.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_6.setBounds(162, 90, 47, 21);
		contentPane.add(panel_6);

		final JPanel panel_7 = new JPanel();
		panel_7.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_7.setBounds(162, 122, 47, 21);
		contentPane.add(panel_7);
		final JComboBox comboBox = new JComboBox();
		final Communicator com = new Communicator() {
			Color[] colors = new Color[8];	
			@Override
			public void serialEvent(SerialPortEvent arg0) {
				// TODO Auto-generated method stub
				try {
					String rx = read_line();
					
					System.out.println(rx);
					
					
					
					
					tokens = rx.split(",");

					for(int i=0; i<8;i++)
					{
						if(Integer.parseInt(tokens[i+1])==1) 
						{
							colors[i] = Color.green;

						}
						else if(Integer.parseInt(tokens[i+1])==0)
						{
							colors[i] = Color.orange;

						}
					}

					panel.setBackground(colors[0]);
					panel_1.setBackground(colors[1]);
					panel_2.setBackground(colors[2]);
					panel_3.setBackground(colors[3]);
					panel_4.setBackground(colors[4]);
					panel_5.setBackground(colors[5]);
					panel_6.setBackground(colors[6]);
					panel_7.setBackground(colors[7]);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}



			}
		};
		
		final ActionListener timer_action = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				com.WriteLine("1\r\n"); //sending command for sensor module 
				
				
			}
		};
		
		final Timer timer = new Timer(1000, timer_action);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent arg0) {
				comboBox.setModel(new DefaultComboBoxModel<String>(com.searchForPorts())); //loading list of serial port 
			}
		});
		
		
		final JSpinner spinner = new JSpinner();
		spinner.setBounds(106, 27, 29, 20);
		contentPane.add(spinner);
		
		final JSpinner spinner_1 = new JSpinner();
		spinner_1.setBounds(106, 58, 29, 20);
		contentPane.add(spinner_1);
		
		final JSpinner spinner_2 = new JSpinner();
		spinner_2.setBounds(106, 90, 29, 20);
		contentPane.add(spinner_2);
		
		final JSpinner spinner_3 = new JSpinner();
		spinner_3.setBounds(106, 122, 29, 20);
		contentPane.add(spinner_3);
		
		final JSpinner spinner_4 = new JSpinner();
		spinner_4.setBounds(219, 27, 29, 20);
		contentPane.add(spinner_4);
		
		final JSpinner spinner_5 = new JSpinner();
		spinner_5.setBounds(219, 59, 29, 20);
		contentPane.add(spinner_5);
		
		final JSpinner spinner_6 = new JSpinner();
		spinner_6.setBounds(219, 91, 29, 20);
		contentPane.add(spinner_6);
		
		final JSpinner spinner_7 = new JSpinner();
		spinner_7.setBounds(219, 122, 29, 20);
		contentPane.add(spinner_7);
		
		
		JButton btnNewButton = new JButton("Po\u0142\u0105cz");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				com.connect((String)comboBox.getSelectedItem()); //select serial port
				com.initIOStream(); //initialize of IO Stream
				com.initListener(); //initialize of event listener 
				
				int[] times = new int[8];
				times[0] = (int) spinner.getValue()*100;
				times[1] = (int) spinner_1.getValue()*100;
				times[2] = (int) spinner_2.getValue()*100;
				times[3] = (int) spinner_3.getValue()*100;
				times[4] = (int) spinner_4.getValue()*100;
				times[5] = (int) spinner_5.getValue()*100;
				times[6] = (int) spinner_6.getValue()*100;
				times[7] = (int) spinner_7.getValue()*100;
				
				
				com.WriteLine("2^"+String.valueOf(times[0])+"^"+String.valueOf(times[1])+"^"+String.valueOf(times[2])+"^"+String.valueOf(times[3])+"^"+String.valueOf(times[4]+"^"+String.valueOf(times[5]+"^"+String.valueOf(times[6])+"^"+String.valueOf(times[7])))+"\r\n"); //send frame with seting for time of hihgh state 
								
				
				
							
				
				
				timer.start();
				
				
				

				







			}
		});
		
	
		
		
		btnNewButton.setBounds(49, 185, 73, 23);
		contentPane.add(btnNewButton);


		comboBox.setBounds(49, 154, 160, 20);
		contentPane.add(comboBox);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(53, 221, 120, 30);
		contentPane.add(lblNewLabel);
		
		
		
		
	}
}
