#include "common.h"

uint16_t set_time[8]={1,1,1,1,1,1,1,1};

void check_simul_PIR(uint8_t i)
{


	if(!pir_lock && !(PINC & (1<<i)))
	{

		pir_state[i] = 1;

	}
	else if((!pir_lock && PINC & (1<<i)) && !Timer1[i])
	{
		Timer1[i] = set_time[i];
		pir_state[i] = 0;
	}
	else if(pir_lock && !(PINC & (1<<i)))
	{
		pir_lock++;


	}

}

void send_frame(void)
{
	uart_puts("0,");
	uint8_t i = 0;
	for(i=0; i<8;i++)
	{
		uart_putint(pir_state[i],10);
		uart_puts(","); //print delimiter
	}
	for(i=0; i<8;i++)
	{

		uart_putint(set_time[i],10);
		uart_puts(",");
	}
	uart_puts("\r\n");

}

void setup(void)
{
	uint8_t i = 0;
	for(i=0; i<8;i++)
	{
		DDRC &= ~(1<<i);
		PORTC |= (1<<i);

	}
}

void set_high_time(char *pbuf) //setting time for hihg state of motion detect
{
	char *wsk;
	char sep[] = "^";
	uint8_t i = 0;
	for(i=0; i<8;i++)
	{
		wsk = strtok_r(NULL,sep,&pbuf);
		set_time[i]  = atoi(wsk);

	}

}

void set_command(char *pbuf)
{
	//	uart_put_string(0,pbuf);
	//	uart_put_string(0,"|");
//#ifdef DEBUG == 1
//	uart_puts(pbuf);
//	uart_puts("\r\n");
//#endif

	char *wsk;
	char sep[] = "^";
	char *rest;


	wsk = strtok_r(pbuf,sep,&rest);





	if(atoi(wsk)==1)
	{

		send_frame();

	}
	else if(atoi(wsk)==2)
	{
		PORTB ^= (1<<0);
		set_high_time(rest);

	}

}
