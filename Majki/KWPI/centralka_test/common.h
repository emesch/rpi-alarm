/*
 * common.h
 *
 *  Created on: 03-11-2014
 *      Author: M.Karda� - ATNEL
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <avr/io.h>
#include "MKUART/mkuart.h"
#include <string.h>

void check_simul_PIR(uint8_t i);
void send_frame(void);
void setup(void);
void set_high_time(char *pbuf);
void set_command(char *pbuf);
void copy_command(char *pbuf);

uint8_t pir_lock;
uint8_t pir_state[8]; //array contain state of PIR sensors

volatile uint16_t Timer1[8];





#endif /* COMMON_H_ */
