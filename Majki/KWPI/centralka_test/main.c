/*
 * main.c
 *
 *  Created on: 04-10-2014
 *      Author: M.Karda� - ATNEL
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "MKUART/mkuart.h"
#include "common.h"

#include <string.h>

#define DEBUG 0


extern volatile uint8_t ascii_line;
extern volatile uint16_t Timer1[8];












char bufor[32];


int main(void)
{
	USART_Init(__UBRR);
	uint8_t i = 0;


	TCCR2 |= (1<<WGM21);
	TCCR2 |= (1<<CS22)|(1<<CS21)|(1<<CS20);
	OCR2 = 108;
	TIMSK |= (1<<OCIE2);

	setup();





	sei();

	while(1)
	{


		for(i=0; i<8;i++)
		{
			check_simul_PIR(i); //check simulate PIR sensors

		}





		if(ascii_line) //chceck for new frame from PC
		{



			uart_gets(bufor);
			set_command(bufor);







		}







	}

}









ISR(TIMER2_COMP_vect)
{
	uint16_t x = 0;

	x = Timer1[0];
	if(x) Timer1[0] = --x;
	x = Timer1[1];
	if(x) Timer1[1] = --x;
	x = Timer1[2];
	if(x) Timer1[2] = --x;
	x = Timer1[3];
	if(x) Timer1[3] = --x;
	x = Timer1[4];
	if(x) Timer1[4] = --x;
	x = Timer1[5];
	if(x) Timer1[5] = --x;
	x = Timer1[6];
	if(x) Timer1[6] = --x;
	x = Timer1[7];
	if(x) Timer1[7] = --x;



}
